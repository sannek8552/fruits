
INSERT INTO `tbl_user` (`id`, `name`, `email`, `password`, `photo`) VALUES
  (1, 'admin', 'admin@dataart.com', '$1$dV3.i93.$Bz/xtrfW3nHbLy03XyuR.1', 'admin.jpg'),
  (3, 'Heizenberg', 'heizenberg@dataart.com', '$1$A.4.Jp1.$PadVyq0iXfaZy/HKVJQbz/', '5480_138608478089.jpg'),
  (2, 'Banana', 'banana@dataart.com', '$1$eB1.P/4.$7ug7wQ.lkmcU8Z/D40/er.', 'banan-300x282.jpg');

INSERT INTO `sourcemessage` (`id`, `category`, `message`) VALUES
(1, 'common', 'Welcome to social network'),
(2, 'common', 'Save'),
(3, 'common', 'Delete'),
(4, 'User', 'Name'),
(5, 'User', 'Email'),
(8, 'User', 'Password'),
(9, 'User', 'Confirm password'),
(10, 'User', 'Photo'),
(11, 'Common', 'Sign up'),
(12, 'Common', 'Fruits Admin Panel'),
(13, 'Common', 'Registration'),
(14, 'Common', 'Users'),
(15, 'Common', 'Logs'),
(16, 'Common', 'Locale'),
(17, 'Common', 'Category'),
(18, 'Common', 'Source Message'),
(19, 'Common', 'Translation'),
(20, 'Common', 'Level'),
(21, 'Common', 'Message'),
(22, 'Common', 'Date'),
(23, 'Common', 'No data found'),
(24, 'Common', 'User list'),
(25, 'Common', 'User profile'),
(26, 'Common', 'User photo'),
(27, 'Common', 'View Profile'),
(28, 'Common', 'Delete'),
(29, 'Common', 'Username'),
(30, 'Common', 'Language'),
(31, 'Common', 'Recipe'),
(32, 'Common', 'Theme'),
(33, 'Common', 'Create new recipe'),
(34, 'Common', 'Like'),
(35, 'Common', 'Unlike'),
(36, 'Common', 'Edit'),
(37, 'Common', 'Add to friends'),
(38, 'Common', 'Remove from friends'),
(39, 'Common', 'Activate'),
(40, 'Common', 'Block'),
(41, 'Common', 'Welcome to admin'),
(42, 'Common', 'Locale settings'),
(43, 'Common', 'Select language'),
(44, 'Common', 'Add new language'),
(45, 'Common', 'Config'),
(46, 'Common', 'Logout'),
(47, 'Common', 'Login'),
(48, 'Common', 'Sign Up'),
(49, 'Common', 'About'),
(50, 'Common', 'About');


INSERT INTO `message` (`id`, `language`, `translation`) VALUES
  (1, 'ch', '憂傷坐艾美特 傷坐艾美特'),
  (1, 'ru', 'Бобро пожаловать в соц сеть'),
  (2, 'ch', '憂坐艾美特'),
  (2, 'ru', 'Сохранить'),
  (3, 'ch', '憂傷坐艾美特'),
  (3, 'ru', 'Удалить'),
  (4, 'ch', '憂傷坐艾美特'),
  (4, 'ru', 'Имя'),
  (5, 'ch', '憂傷坐艾美特'),
  (5, 'ru', 'Email'),
  (8, 'ch', '憂傷坐艾美特'),
  (8, 'ru', 'Пароль'),
  (9, 'ch', '憂傷坐艾美特'),
  (9, 'ru', 'Повторить пароль'),
  (10, 'ch', '憂傷坐艾美特'),
  (10, 'ru', 'Фото'),
  (11, 'ch', '憂傷艾美特'),
  (11, 'ru', 'Зарегистрироваться'),
  (12, 'ch', '憂傷坐艾特'),
  (12, 'ru', 'Админка'),
  (13, 'ch', '憂傷艾美特'),
  (13, 'ru', 'Регистрация'),
  (14, 'ch', '憂傷坐艾美特'),
  (14, 'ru', 'Пользователи'),
  (15, 'ch', '憂傷坐艾美特'),
  (15, 'ru', 'Логи'),
  (16, 'ch', '憂傷坐艾美特'),
  (16, 'ru', 'Переводы'),
  (17, 'ch', '憂傷坐艾美特'),
  (17, 'ru', 'Категория'),
  (18, 'ch', '憂傷坐艾美特'),
  (18, 'ru', 'Исходное сообщение'),
  (19, 'ch', '憂傷坐艾美特'),
  (19, 'ru', 'Перевод'),
  (20, 'ch', '憂傷坐艾美特'),
  (20, 'ru', 'Уровень'),
  (21, 'ch', '憂傷坐艾特'),
  (21, 'ru', 'Сообщение'),
  (22, 'ch', '憂傷美特'),
  (22, 'ru', 'Дата'),
  (23, 'ch', '坐艾美特'),
  (23, 'ru', 'Данные не найдены'),
  (24, 'ch', '憂傷坐艾美特'),
  (24, 'ru', 'Список пользователей'),
  (25, 'ch', '憂艾美特'),
  (25, 'ru', 'Профиль пользователя'),
  (26, 'ch', '憂傷坐艾美特'),
  (26, 'ru', 'Фото пользователя'),
  (27, 'ch', '憂傷坐艾美特'),
  (27, 'ru', 'Смотреть профиль'),
  (28, 'ch', '憂傷坐艾美特'),
  (28, 'ru', 'Удалить'),
  (29, 'ch', '憂傷坐艾美特'),
  (29, 'ru', 'Имя пользователя'),
  (30, 'ch', '憂傷坐艾美特'),
  (30, 'ru', 'Язык'),
  (31, 'ch', '憂傷坐艾美'),
  (31, 'ru', 'Рецепт'),
  (32, 'ch', '憂傷坐艾美特'),
  (32, 'ru', 'Тема'),
  (33, 'ch', '憂傷艾美特'),
  (33, 'ru', 'Создать новый рецепт'),
  (34, 'ch', '憂傷坐艾美特'),
  (34, 'ru', 'Мне нравится'),
  (35, 'ch', '憂傷坐艾美特'),
  (35, 'ru', 'Мне не нравится'),
  (36, 'ch', '憂傷坐艾特'),
  (36, 'ru', 'Редактировать'),
  (37, 'ch', '傷坐艾美特'),
  (37, 'ru', 'Добавить в друзья'),
  (38, 'ch', '傷坐艾美特 艾美特'),
  (38, 'ru', 'Удалить из друзей'),
  (39, 'ch', '憂傷美特'),
  (39, 'ru', 'Активировать'),
  (40, 'ch', '憂傷'),
  (40, 'ru', 'Заблокировать'),
  (41, 'ch', '憂傷坐艾美 坐艾美'),
  (41, 'ru', 'Добро пожаловать в админку'),
  (42, 'ch', '傷坐艾美特'),
  (42, 'ru', 'Настройки переводов'),
  (43, 'ch', '憂傷坐艾美特'),
  (43, 'ru', 'Выбрать язык'),
  (44, 'ch', '憂傷坐艾美特'),
  (44, 'ru', 'Добавить язык'),
  (45, 'ch', '傷坐艾美特'),
  (45, 'ru', 'Настройки'),
  (46, 'ch', '憂傷坐艾'),
  (46, 'ru', 'Выйти'),
  (47, 'ch', '憂美特'),
  (47, 'ru', 'Войти'),
  (48, 'ch', '憂傷坐艾'),
  (48, 'ru', 'Зарегистрироваться'),
  (49, 'ch', '憂傷坐艾美特'),
  (49, 'ru', 'О сайте'),
  (50, 'ch', '憂傷坐');


INSERT INTO `tbl_config` (`id`, `name`, `value`) VALUES
(1, 'theme', 'flatly');

--
-- Дамп данных таблицы `tbl_language`
--

INSERT INTO `tbl_language` (`language`, `language_str`) VALUES
('ch', 'Chinese'),
('en', 'English'),
('ru', 'Русский');

--
-- Дамп данных таблицы `tbl_recipe`
--

INSERT INTO `tbl_recipe` (`id`, `user_id`, `title`, `text`, `photo`, `date`) VALUES
(1, 1, 'Арбуз', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.', 'arbuz.jpg', '2014-06-09 18:08:34'),
(2, 1, 'Фруктовое ассорти', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.', 'fruit1.jpg', '2014-06-09 18:08:34'),
(3, 2, 'Киви и лимон', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.', 'lemon.jpg', '2014-06-09 18:08:34'),
(4, 3, 'Салат вегетерианский', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.', 'veget1.jpg', '2014-06-09 18:08:34'),
(5, 3, 'Вегетерианский 2', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.', 'veget2.jpg', '2014-06-09 18:08:34');

--
-- Дамп данных таблицы `tbl_recipe_comment`
--

INSERT INTO `tbl_recipe_comment` (`id`, `recipe_id`, `user_id`, `text`, `date`) VALUES
(1, 1, 1, 'Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации "Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст.." Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).', '2014-06-06 00:00:00'),
(2, 1, 2, '2354', '2014-06-10 16:54:06'),
(3, 2, 3, '534235234', '2014-06-10 16:58:22'),
(4, 2, 1, '4324234', '2014-06-10 18:02:09'),
(5, 3, 2, '123', '2014-06-10 18:08:27'),
(6, 4, 3, 'Test', '2014-06-18 11:48:13'),
(7, 5, 1, '5423', '2014-06-18 11:49:41');

--
-- Дамп данных таблицы `tbl_recipe_like`
--

INSERT INTO `tbl_recipe_like` (`user_id`, `recipe_id`) VALUES
(1, 1),
(2, 1),
(3, 2),
(1, 2),
(2, 3),
(3, 3),
(1, 3);


INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
  ('addComment', 0, 'Добавить комментарий', NULL, 'N;'),
  ('addRecipe', 0, NULL, NULL, NULL),
  ('admin', 2, '', NULL, 'N;'),
  ('blocked', 2, NULL, NULL, NULL),
  ('blockUser', 0, 'Block User', NULL, NULL),
  ('createUser', 0, NULL, NULL, NULL),
  ('deleteComment', 0, NULL, NULL, NULL),
  ('deleteRecipe', 0, NULL, NULL, NULL),
  ('deleteUser', 0, NULL, NULL, NULL),
  ('moderator', 2, NULL, NULL, NULL),
  ('setTheme', 0, 'Изменить тему', NULL, 'N;'),
  ('user', 2, '', NULL, 'N;');

INSERT INTO `authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
  ('admin', 1, NULL, 'N;'),
  ('moderator', 2, NULL, NULL),
  ('user', 3, NULL, 'N;');

INSERT INTO `authitemchild` (`parent`, `child`) VALUES
  ('user', 'addComment'),
  ('user', 'addRecipe'),
  ('user', 'blocked'),
  ('admin', 'blockUser'),
  ('moderator', 'blockUser'),
  ('admin', 'createUser'),
  ('moderator', 'deleteComment'),
  ('moderator', 'deleteRecipe'),
  ('admin', 'deleteUser'),
  ('admin', 'moderator'),
  ('admin', 'setTheme'),
  ('moderator', 'user');

