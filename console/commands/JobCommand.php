<?php

/**
 * Class JobCommand
 */
class JobCommand extends CConsoleCommand
{
    //in future we can extend settings with lastmod and other params
    protected $urlMap = array(
        array(
            'loc' => '/'
        ),
        array(
            'loc' => '/users'
        ),
        array(
            'loc' => '/recipe'
        ),
        array(
            'loc' => '/recipe/create'
        ),
        array(
            'loc' => '/auth/login'
        ),
        array(
            'loc' => '/auth/signup'
        ),
    );

    public function actionSitemap()
    {
        $baseUrl = Yii::app()->request->getHostInfo();
        $xml = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
        $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">' . PHP_EOL;
        foreach ($this->urlMap as $url) {
            $xml .= '<url>' . PHP_EOL;
            $xml .= '<loc>' . $baseUrl . '' . $url['loc'] . '</loc>' . PHP_EOL;
            $xml .= '<lastmod>' . date('Y-m-d') . '</lastmod>' . PHP_EOL;
            $xml .= '<changefreq>monthly</changefreq>' . PHP_EOL;
            $xml .= '<priority>0.5</priority>' . PHP_EOL;
            $xml .= '</url>' . PHP_EOL;
        }

        /** @var User[] $users */
        $users = User::model()->findAll();
        foreach ($users as $user) {
            $xml .= '<url>' . PHP_EOL;
            $xml .= '<loc>' . $baseUrl . '/u/' . $user->name . '</loc>' . PHP_EOL;
            $xml .= '<image:image>' . PHP_EOL;
            $xml .= '<image:loc>' . $baseUrl . '/thumbs/avatar/150x150/' . $user->photo . '</image:loc>' . PHP_EOL;
            $xml .= '<image:title>' . $user->name . '</image:title>' . PHP_EOL;
            $xml .= '</image:image>' . PHP_EOL;
            $xml .= '<lastmod>' . date('Y-m-d') . '</lastmod>' . PHP_EOL;
            $xml .= '<changefreq>monthly</changefreq>' . PHP_EOL;
            $xml .= '<priority>0.5</priority>' . PHP_EOL;
            $xml .= '</url>' . PHP_EOL;
        }

        $xml .= '</urlset>' . PHP_EOL;

        echo $xml;
    }

    public function actionEmail()
    {
        /** @var User[] $users */
        $users = User::model()->findAll();

        $email = Yii::app()->email;

        foreach ($users as $user) {
            $email->to = $user->email;
            $email->subject = '[SPAM] for User ' . $user->name;
            $email->message = 'Hello brother! This is SPAAAAAAAAAARTAAAAAAAAAAAAAAAAAAAA!!!!!!!!!!!!!!!!!';
            $email->send();
        }
    }

} 
