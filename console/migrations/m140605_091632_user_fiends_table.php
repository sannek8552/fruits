<?php

class m140605_091632_user_fiends_table extends CDbMigration
{

    public function safeUp()
    {
        $this->createTable(
            'tbl_user_friends',
            array(
                'user_id'   => 'INTEGER(10) unsigned',
                'friend_id' => 'INTEGER(10) unsigned',
            )
        );

        $this->addPrimaryKey('PK_tbl_user_friends', 'tbl_user_friends', 'user_id, friend_id');

        $this->addForeignKey(
            'FK_User_User_friends1',
            'tbl_user_friends',
            'user_id',
            'tbl_user',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'FK_User_User_friends2',
            'tbl_user_friends',
            'friend_id',
            'tbl_user',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_User_User_friends1', 'tbl_user_friends');
        $this->dropForeignKey('FK_User_User_friends2', 'tbl_user_friends');
        $this->dropTable('tbl_user_friends');
    }

}
