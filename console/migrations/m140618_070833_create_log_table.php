<?php

class m140618_070833_create_log_table extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            'tbl_log',
            array(
                'id'       => 'INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'level'    => 'VARCHAR(128)',
                'category' => 'VARCHAR(128)',
                'logtime'  => 'INTEGER',
                'message'  => 'text',
            )
        );
    }

    public function safeDown()
    {
        $this->dropTable('tbl_log');
    }
}
