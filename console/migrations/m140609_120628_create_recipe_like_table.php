<?php

class m140609_120628_create_recipe_like_table extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            'tbl_recipe_like',
            array(
                'user_id'   => 'INTEGER(10) unsigned',
                'recipe_id' => 'INTEGER(10) unsigned',
            )
        );

        $this->addPrimaryKey('PK_tbl_recipe_like', 'tbl_recipe_like', 'user_id, recipe_id');

        $this->addForeignKey(
            'FK_tbl_recipe_like_tbl_user',
            'tbl_recipe_like',
            'user_id',
            'tbl_user',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'FK_tbl_recipe_like_tbl_recipe',
            'tbl_recipe_like',
            'recipe_id',
            'tbl_recipe',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_tbl_recipe_like_tbl_user', 'tbl_recipe_like');
        $this->dropForeignKey('FK_tbl_recipe_like_tbl_recipe', 'tbl_recipe_like');
        $this->dropTable('tbl_recipe_like');
    }
}
