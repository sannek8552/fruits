<?php

class m140619_085626_create_language_table extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            'tbl_language',
            array(
                'language'     => 'VARCHAR(16) PRIMARY KEY',
                'language_str' => 'VARCHAR(255)',
            )
        );
    }

    public function safeDown()
    {
        $this->dropTable('tbl_language');
    }
}
