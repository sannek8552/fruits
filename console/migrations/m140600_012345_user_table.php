<?php

class m140600_012345_user_table extends CDbMigration
{
    public function up()
    {
        $this->createTable(
            'tbl_user',
            array(
                'id'       => 'INTEGER(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'name'     => 'VARCHAR(255)',
                'email'    => 'VARCHAR(255)',
                'password' => 'VARCHAR(255)',
                'photo'    => 'VARCHAR(255)',
            )
        );

    }

    public function down()
    {
        $this->dropTable('tbl_user_friends');
    }

}
