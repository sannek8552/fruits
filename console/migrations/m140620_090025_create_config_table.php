<?php

class m140620_090025_create_config_table extends CDbMigration
{
    public function up()
    {
        $this->createTable(
            'tbl_config',
            array(
                'id'    => 'INTEGER(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY',
                'name'  => 'VARCHAR(40) NOT NULL',
                'value' => 'VARCHAR(255) NOT NULL',
            )
        );

        $this->createIndex('tbl_config_name_idx', 'tbl_config', 'name', true);
    }

    public function down()
    {
        $this->dropTable('tbl_config');
    }
}
