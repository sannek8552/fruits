<?php

class m140616_081643_create_auth_structure extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            'AuthItem',
            array(
                'name'        => 'varchar(64) not null',
                'type'        => 'integer not null',
                'description' => 'text',
                'bizrule'     => 'text',
                'data'        => 'text',
            )
        );

        $this->addPrimaryKey('PK_AuthItem', 'AuthItem', 'name');

        $this->createTable(
            'AuthItemChild',
            array(
                'parent' => 'varchar(64) not null',
                'child'  => 'varchar(64) not null',
            )
        );

        $this->addPrimaryKey('PK_AuthItemChild', 'AuthItemChild', 'parent, child');

        $this->addForeignKey(
            'FK_AuthItemChild_AuthItem',
            'AuthItemChild',
            'parent',
            'AuthItem',
            'name',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK_AuthItemChild_AuthItem_Child',
            'AuthItemChild',
            'child',
            'AuthItem',
            'name',
            'CASCADE',
            'CASCADE'
        );


        $this->createTable(
            'AuthAssignment',
            array(
                'itemname' => 'varchar(64) not null',
                'userid'   => 'INTEGER(10) unsigned not null',
                'bizrule'  => 'text',
                'data'     => 'text',
            )
        );

        $this->addPrimaryKey('PK_AuthAssignment', 'AuthAssignment', 'itemname, userid');

        $this->addForeignKey(
            'FK_AuthAssignment_AuthItem',
            'AuthAssignment',
            'itemname',
            'AuthItem',
            'name',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK_AuthAssignment_User',
            'AuthAssignment',
            'userid',
            'tbl_user',
            'id',
            'CASCADE',
            'RESTRICT'
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_AuthAssignment_User', 'AuthAssignment');
        $this->dropForeignKey('FK_AuthAssignment_AuthItem', 'AuthAssignment');
        $this->dropForeignKey('FK_AuthItemChild_AuthItem', 'AuthItemChild');
        $this->dropForeignKey('FK_AuthItemChild_AuthItem_Child', 'AuthItemChild');
        $this->dropTable('AuthAssignment');
        $this->dropTable('AuthItemChild');
        $this->dropTable('AuthItem');
    }

}
