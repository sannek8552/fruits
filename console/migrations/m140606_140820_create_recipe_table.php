<?php

class m140606_140820_create_recipe_table extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            'tbl_recipe',
            array(
                'id' => 'INTEGER(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'user_id' => 'INTEGER(10) unsigned',
                'title' => 'VARCHAR(255)',
                'text' => 'TEXT',
                'photo' => 'VARCHAR(255)',
                'date' => 'DATETIME',
            )
        );

        $this->addForeignKey(
            'FK_Recipe_User',
            'tbl_recipe',
            'user_id',
            'tbl_user',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_Recipe_User', 'tbl_recipe');
        $this->dropTable('tbl_recipe');
    }

}
