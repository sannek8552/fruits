<?php

class m140610_100424_create_comment_table extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            'tbl_recipe_comment',
            array(
                'id' => 'INTEGER(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'recipe_id' => 'INTEGER(10) unsigned',
                'user_id' => 'INTEGER(10) unsigned',
                'text' => 'TEXT',
                'date' => 'DATETIME',
            )
        );

        $this->addForeignKey(
            'FK_Recipe_comment_Recipe',
            'tbl_recipe_comment',
            'recipe_id',
            'tbl_recipe',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'FK_Recipe_comment_User',
            'tbl_recipe_comment',
            'user_id',
            'tbl_user',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_Recipe_comment_User', 'tbl_recipe_comment');
        $this->dropForeignKey('FK_Recipe_comment_Recipe', 'tbl_recipe_comment');
        $this->dropTable('tbl_recipe_comment');
    }
}
