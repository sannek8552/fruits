<?php


class Permission {
    const EDIT_RECIPE    = 'addRecipe';
    const DELETE_RECIPE = 'deleteRecipe';


    const DELETE_COMMENT = 'deleteComment';


    const BLOCK_USER    = 'blockUser';
    const CREATE_USER   = 'createUser';
    const DELETE_USER   = 'deleteUser';


    const SET_THEME    = 'setTheme';
}
