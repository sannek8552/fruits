<?php

class ActiveRecord extends CActiveRecord
{
    /**
     * @param array $fields
     * @param null $id
     *
     * @return bool
     */
    public function isDuplicate(array $fields, $id = null)
    {
        $condition = array();
        $params = array();
        if ($id) {
            $condition = 'id != :currentID';
            $params = array(':currentID' => $id);
        }

        return !!static::model()->findByAttributes($fields, $condition, $params);
    }
} 
