<?php

abstract class BaseBackendController extends EController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'roles' => array('admin', 'moderator'),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }
}
