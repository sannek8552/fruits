<?php
class LogoutAction extends CAction
{
    public function run()
    {
        if (!Yii::app()->user->isGuest) {
            Yii::app()->user->logout();
        }

        $this->getController()->redirect('/home');
    }
}
