<?php
class LoginAction extends CAction
{
    public function run()
    {
        /** @var EController $controller */
        $controller = $this->getController();
        if (!Yii::app()->user->isGuest) {
            $controller->redirect('/home');
        }
        $user = new User();
        if (Yii::app()->request->isPostRequest) {
            $user->attributes = $_POST['form'];

            $identity = new UserIdentity($user->email, $user->password);
            if ($identity->authenticate()) {
                Yii::app()->user->login($identity);
                $controller->redirect('/home');
            } else {
                switch ($identity->errorCode) {
                    case UserIdentity::ERROR_PASSWORD_INVALID:
                        $errorField = 'password';
                        break;
                    default:
                        $errorField = 'email';
                        break;
                }
                $user->addError($errorField, $identity->errorMessage);
            }
        }

        $controller->render('login', array(
                'user' => $user,
            ));
    }
}
