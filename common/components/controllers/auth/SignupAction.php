<?php
class SignupAction extends CAction
{
    public function run()
    {
        /** @var EController $controller */
        $controller = $this->getController();

        //only guests and admins can go to registration page
        if (!Yii::app()->user->isGuest && !Yii::app()->user->isAdmin()) {
            $controller->redirect('/home');
        }

        $assign = array();
        if (Yii::app()->request->isPostRequest) {
            $user = new User('signup');
            $user->attributes = $_POST['User'];
            $user->cpassword = isset($_POST['User']['cpassword']) ? $_POST['User']['cpassword'] : '';
            $user->role = isset($_POST['User']['role']) ? $_POST['User']['role'] : '';

            if ($user->register()) {
                $controller->setSuccessMessage('Welcome! Please, log in!');
                $controller->redirect('/auth/login');
            }
        } else {
            $user = new User();
        }
        $assign['user'] = $user;
        $controller->render('signup', $assign);
    }
}
