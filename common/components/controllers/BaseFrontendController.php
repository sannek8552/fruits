<?php

abstract class BaseFrontendController extends EController
{
    /** @var Language[]|null */
    public $languageList = null;

    protected $config = null;

    public function init()
    {
        parent::init();
        $this->initTheme();

        $this->languageList = Language::model()->findAll();
    }

    protected function initTheme()
    {
        if ($theme = $this->getConfig('theme')) {
            Yii::app()->theme = $theme;
        }
    }

    /**
     * @param string|null $name
     *
     * @return array|string
     */
    public function getConfig($name = null)
    {
        if ($this->config === null) {
            /** @var Config[] $items */
            $items = Config::model()->findAll();
            foreach ($items as $item) {
                $this->config[$item->name] = $item->value;
            }
        }

        if ($name && isset($this->config[$name])) {
            return $this->config[$name];
        }

        return $this->config;
    }


}
