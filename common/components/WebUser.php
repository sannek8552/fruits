<?php

/**
 * Class WebUser
 */
class WebUser extends CWebUser
{
    /** @var User|null */
    protected $model = null;

    /**
     * @return User
     */
    public function getModel()
    {
        if (!$this->isGuest && $this->model === null) {
            $this->model = User::model()->loadWithFriends($this->id);
        }

        return $this->model;
    }

    public function isAdmin()
    {
        return $this->checkAccess('admin');
    }

    protected function afterLogin($fromCookie)
    {
        Logger::info(sprintf("User (ID: %d) logged on", $this->id), 'common.components.WebUser');
        parent::afterLogin($fromCookie);
    }

    protected function beforeLogout()
    {
        Logger::info(sprintf("User (ID: %d) logged out", $this->id), 'common.components.WebUser');
        return parent::beforeLogout();
    }


} 
