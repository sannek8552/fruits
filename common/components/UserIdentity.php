<?php

class UserIdentity extends CUserIdentity
{
    private $_id;

    /** @var  String */
    public $email;

    /** @var  String */
    public $name;

    /**
     * @param string $email
     * @param string $password
     */
    public function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }


    /**
     * @return bool
     */
    public function authenticate()
    {
        /** @var User $record */
        $record = User::model()->findByAttributes(array('email' => $this->email));
        if ($record === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            $this->errorMessage = 'Email is invalid';
        } else {
            if (!$this->isPasswordOK($record)) {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
                $this->errorMessage = 'Password is invalid';
            } else {
                $this->_id = $record->id;
                $this->name = $record->name;
                $this->errorCode = self::ERROR_NONE;
            }
        }
        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @param User $record
     *
     * @return bool
     */
    protected function isPasswordOK(User $record)
    {
        return User::checkPassword($this->password, $record->password);
    }
}
