<?php
/**
 *
 * main.php configuration file
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
return array(
    'id' => 'fruits',
	'aliases' => array(
		'frontend' => dirname(__FILE__) . '/../..' . '/frontend',
		'common' => dirname(__FILE__) . '/../..' . '/common',
		'console' => dirname(__FILE__) . '/../..' . '/console',
		'backend' => dirname(__FILE__) . '/../..' . '/backend',
		'vendor' => 'common.lib.vendor',
		'resizer' => 'common.extensions.resizer',
	),
    'sourceLanguage' => 'en',
    'language' => 'ru',
    'timeZone' => 'Europe/Moscow',

	'import' => array(
		'common.extensions.components.*',
		'common.extensions.*',
		'common.components.*',
		'common.components.controllers.*',
		'common.helpers.*',
		'common.models.*',
		'common.views.*',
		'common.widgets.*',
		'application.controllers.*',
		'application.extensions.*',
		'application.helpers.*',
		'application.models.*',
		'vendor.2amigos.yiistrap.helpers.*',
		'vendor.2amigos.yiiwheels.helpers.*',
	),
	'components' => array(
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
        'user' => array(
            'allowAutoLogin' => true,
            'class' => 'WebUser',
            'loginUrl'=>array('auth/login'),
        ),
        'authManager'=>array(
            'class'=>'CDbAuthManager',
            'connectionID'=>'db',
        ),
        'messages' => array(
            'class' => 'CDbMessageSource',
        ),
        'log' => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'        => 'CDbLogRoute',
                    'logTableName' => 'tbl_log',
                    'connectionID' => 'db',
                    'levels'       => 'info, error',
                    'categories'   => array(
                        'application.*',
                        'common.*',
                        'frontend.*',
                        'backend.*',
                    ),
                ),
                array(
                    'class'  => 'CEmailLogRoute',
                    'levels' => 'error',
                    'emails' => 'test@email.com',
                ),
            ),
        ),
        'email'=>array(
            'class'=>'common.extensions.email.Email',
            'delivery'=>'php',
        ),
	),
	'params' => array(
		// php configuration
		'php.defaultCharset' => 'utf-8',
		'php.timezone'       => 'UTC',
        'developerEmailAddress' => 'test@mail.com'
	)
);
