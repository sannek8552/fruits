<?php

/**
 * This is the model class for table "tbl_user".
 *
 * The followings are the available columns in table 'tbl_user':
 * @property string $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $photo
 *
 * @property User[]|null $friends
 * @property Recipe[] $recipes
 * @property Recipe[] $recipeLikes
 * @property RecipeComment[] $recipeComments
 */
class User extends ActiveRecord
{
    public $cpassword;

    public $role;

    public $photo;

    public $friends = null;

    protected $roles = null;

    protected $permissions = null;

    const PASSWORD_SALT = '!$#JDKA';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}

    /**
     * @param string $email
     * @param string $password
     *
     * @return User
     */
    public function loadByEmailAndPassword($email, $password)
    {
        $password = $this->hashPassword($password);

        return $this->findByAttributes(array(
            'email'    => $email,
            'password' => $password,
        ));
    }

    /**
     * @param string $password
     *
     * @return mixed
     */
    public static function hashPassword($password)
    {
        return crypt($password);
    }

    public static function checkPassword($password, $hash)
    {
        return $hash === crypt($password, $hash);
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, email, password, photo', 'required', 'on' => 'signup'),
			array('name, email', 'length', 'max'=>255),
            array('name', 'match', 'pattern'=>'/^[\w\s,]+$/', 'message'=>'Name should have only letters'),
			array('password', 'length', 'max'=>40),
			array('email', 'email'),
			array('password', 'compare', 'compareAttribute'=>'cpassword', 'on' => 'signup'),
            array('photo', 'file', 'types'=>'jpg, jpeg, gif, png'),
			array('id, name, email, password, photo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
            'recipes' => array(self::HAS_MANY, 'Recipe', 'user_id', 'index' => 'id', 'order'=>'date DESC'),
            'recipeComments' => array(self::HAS_MANY, 'RecipeComment', 'user_id', 'index' => 'id', 'order'=>'date DESC'),
            'recipeLikes' => array(self::MANY_MANY, 'Recipe', 'tbl_recipe_like(user_id, recipe_id)', 'index' => 'id'),
		);
	}

//    public function behaviors(){
//        return array( 'CAdvancedArBehavior' => array(
//            'class' => 'frontend.extensions.CAdvancedArBehavior'));
//    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'email' => 'Email',
			'password' => 'Password',
			'photo' => 'Photo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('photo',$this->photo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function register()
    {
        $fileInstance = CUploadedFile::getInstance($this, 'photo');
        $fileName = $fileInstance ? $fileInstance->name : '';
        $uploadDir = Yii::getPathOfAlias('frontend') . '/www/upload/';
        $this->photo = Helpers::getUniqueFileName($uploadDir, $fileName);
        if (!$this->validate()) {
            return false;
        }

        $trans = $this->dbConnection->beginTransaction();
        $result = false;
        try {
            $this->password = self::hashPassword($this->password);

            if ($this->save(false)) {
                Yii::app()->authManager->assign($this->role, $this->id);
                $trans->commit();

                $fileInstance->saveAs($uploadDir . $this->photo);
                $result = true;
                Yii::log(get_class($this) . ' register()', CLogger::LEVEL_INFO, 'common.model.User');
            }
        } catch (Exception $e) {
            $trans->rollback();
            throw $e;
        }

        return $result;
    }

    protected function afterValidate()
    {
        if ($this->isEmailExists($this->email, $this->id)) {
            $this->addError('email', Yii::t('User', 'Email already exists'));
        }
        if ($this->isNameExists($this->name, $this->id)) {
            $this->addError('name', Yii::t('User', 'User name already exists'));
        }

        //user role
        if (Yii::app()->user->isAdmin()) {
            $availableRoles = Yii::app()->authManager->getAuthItems(CAuthItem::TYPE_ROLE);
            if (!isset($availableRoles[$this->role])) {
                $this->role = 'user';
            }
        } else {
            $this->role = 'user';
        }

        parent::afterValidate();
    }

    /**
     * @param      $email
     * @param null $id
     *
     * @return bool
     */
    public function isEmailExists($email, $id = null)
    {
        $fields = array(
            'email' => $email
        );

        $condition = array();
        $params = array();
        if ($id) {
            $condition = 'id != :currentID';
            $params = array(':currentID' => $id);
        }

        return !!User::model()->findByAttributes($fields, $condition, $params);
    }

    /**
     * @param      $name
     * @param null $id
     *
     * @return bool
     */
    public function isNameExists($name, $id = null)
    {
        $fields = array(
            'name' => $name
        );

        $condition = array();
        $params = array();
        if ($id) {
            $condition = 'id != :currentID';
            $params = array(':currentID' => $id);
        }

        return !!User::model()->findByAttributes($fields, $condition, $params);
    }

    /**
     * @param int  $friendId
     * @param bool $isFriend
     *
     * @throws CHttpException
     * @throws Exception
     */
    public function addFriend($friendId, $isFriend = true)
    {
        $friend = User::model()->findByPk($friendId);
        if (!$friend) {
            throw new CHttpException(404, 'User not found');
        }

        $trans = $this->dbConnection->beginTransaction();
        try {

            if (!$isFriend) {
                /** @var  UserFriend $userFriendModel */
                $userFriendModel = UserFriend::model();
                /** @var UserFriend[] $userFriends */
                $userFriends = $userFriendModel->loadFriends($this->id, $friendId);
                foreach ($userFriends as $friend) {
                    $friend->delete();
                }
                Logger::info(sprintf("User (ID: %d) delete friend (ID: %d)", $this->id, (int)$friendId), 'common.model.UserFriend');
            } else {
                /** @var UserFriend $userFriend */
                $userFriend = new UserFriend();
                $userFriend->user_id = $this->id;
                $userFriend->friend_id = $friendId;
                $userFriend->save();
                Logger::info(sprintf("User (ID: %d) add friend (ID: %d)", $this->id, (int)$friendId), 'common.model.UserFriend');
            }

            $trans->commit();
        } catch (Exception $e) {
            $trans->rollback();
            throw $e;
        }
    }

    /**
     * @param int $id
     *
     * @return User
     * @throws CHttpException
     */
    public function loadWithFriends($id)
    {

        /** @var User $user */
        $user = User::model()->findByPk($id);
        if (!$user) {
            throw new CHttpException(404, 'User not found');
        }

        $friendIds = Yii::app()->db->createCommand()
            ->select('IF(uf.user_id = :userId, uf.friend_id, uf.user_id) AS friend_id')
            ->from('tbl_user u')
            ->join('tbl_user_friends uf', '(uf.user_id = :userId OR uf.friend_id = :userId)')
            ->where('id=:userId'/*, array(':userId'=>$id)*/)
            ->bindParam(':userId', $id)
            ->queryAll();

        $ids = array_map(function ($item) {return (int)$item['friend_id'];}, $friendIds);
        $userFriends = User::findAllByPk($ids, array('index' => 'id'));

        $user->friends = $userFriends;

        return $user;
    }

    /**
     * @param int $userId
     *
     * @return bool
     * @throws CHttpException
     */
    public function block($userId)
    {
        /** @var User $user */
        $user = $this->findByPk((int)$userId);
        if (!$user) {
            throw new CHttpException(404, Yii::t('Common', 'User not found'));
        }

        /** @var CDbAuthManager $auth */
        $auth = Yii::app()->authManager;

        if ($user->hasRole('admin') || $user->hasRole('moderator')) {
            throw new CHttpException(403);
        }

        if ($user->hasRole('blocked')) {
            Logger::info("Activate user ID:" . $user->id, 'common.model.User');
            $auth->revoke('blocked', $user->id);
            if (!$user->hasRole('user')) {
                $auth->assign('user', $user->id);
            }
            $isBlocked = false;
        } else {
            Logger::info("Block user ID:" . $user->id, 'common.model.User');
            $auth->assign('blocked', $user->id);
            $isBlocked = true;
        }

        return $isBlocked;
    }

    /**
     * @return null
     */
    public function getRoles()
    {
        if ($this->id && $this->roles === null) {
            /** @var CDbAuthManager $auth */
            $auth = Yii::app()->authManager;
            $this->roles = $auth->getRoles($this->id);
        }

        return $this->roles;
    }

    /**
     * @return null
     */
    public function getPermissions()
    {
        if ($this->id && $this->permissions === null) {
            /** @var CDbAuthManager $auth */
            $auth = Yii::app()->authManager;
            $this->permissions = $auth->getAuthItems(CAuthItem::TYPE_OPERATION, $this->id);
        }

        return $this->permissions;
    }

    /**
     * @param string $role
     *
     * @return bool
     */
    public function hasRole($role)
    {
        $roles = $this->getRoles();

        return isset($roles[$role]);
    }

    /**
     * @param string $permission
     *
     * @return bool
     */
    public function hasPermission($permission)
    {
        $permissions = $this->getPermissions();

        return isset($permissions[$permission]);
    }

    protected function afterDelete()
    {
        Logger::info(sprintf("User (ID: %d) deleted", $this->id), 'common.model.User');
        parent::afterDelete();
    }


}
