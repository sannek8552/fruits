<?php

/**
 * Class Logger
 */
class Logger
{
    /**
     * @param string $msg
     * @param string $category
     */
    public static function info($msg, $category = 'application')
    {
        Yii::log($msg, CLogger::LEVEL_INFO, $category);
    }

    /**
     * @param string $msg
     * @param string $category
     */
    public static function error($msg, $category = 'application')
    {
        Yii::log($msg, CLogger::LEVEL_ERROR, $category);
    }

    /**
     * @param string $msg
     * @param string $category
     */
    public static function warning($msg, $category = 'application')
    {
        Yii::log($msg, CLogger::LEVEL_WARNING, $category);
    }
} 
