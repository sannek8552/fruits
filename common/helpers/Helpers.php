<?php

class Helpers {

    /**
     * @param $dir
     * @param $filename
     *
     * @return string
     */
    public static function getUniqueFileName($dir, $filename)
    {
        return md5($dir . $filename . time());

        /*if (!file_exists($dir . $filename)) {
            return $filename;
        }

        $result = false;
        $parts = explode('.', $filename);
        $ext = array_pop($parts);
        $filename = implode('.', $parts);
        $newFileName = '';

        $i = 0;
        while (!$result) {
            $i++;
            $newFileName = $filename . "($i)." . $ext;
            if (!file_exists($dir .$newFileName)) {
                $result = true;
            }
        }

        return $newFileName;*/
    }

    /**
     * @param $date
     *
     * @return string
     */
    public static function dateToAgoFormat($date)
    {
        $datetime = strtotime($date);
        $now = time();

        $difference = $now - $datetime;

        if ($difference < 60) {
            return Yii::t('Common', 'Less than a minute ago');
        } else if ($difference >= 60 && $difference < 3600) {
            return Yii::t('Common', 'About {n} minute|{n} minutes ago', round($difference / 60));
        } else if ($difference >= 3600 && $difference < 86400) {
            return Yii::t('Common', 'About {n} hour|{n} hours ago', round($difference / 3600));
        } else if ($difference >= 86400 && $difference < 2592000) {
            return Yii::t('Common', 'About {n} day|{n} days ago', round($difference / 86400));
        } else if ($difference >= 2592000 && $difference < 31536000) {
            return Yii::t('Common', 'About {n} month|{n} months ago', round($difference / 2592000));
        } else {
            return Yii::t('Common', 'About {n} year|{n} years ago', round($difference / 31536000));
        }
    }
} 
