﻿Социальная сесть FRUITS
=======
Тут должно быть описание соц сети...


## Установка и первый запуск

 * Создайте и настройте конфигурационный файл  `common/config/local.php`. За основу можно взять файл `common/config/local.php.sample`.
 * Запустите команду `composer self-update` чтобы обновить composer.
 * Запустите команду `composer install` чтобы развернуть проект.
 * Примените  дамп `dbdump.sql`.
 * Директория `frontend/upload` должна иметь права на запись.
 * Чтобы получить доступ до панели администратора настройте виртуальный хост
```
    <VirtualHost 127.0.0.1:80>
        DocumentRoot "ПУТЬ_ДО_ВАШЕГО_ПРОЕКТА/backend/www"
        ServerName "admin.{ВАШ_ДОМЕН}"
        ServerAlias "admin.{ВАШ_ДОМЕН}"
    </VirtualHost>
```
 * По умолчанию доступны пользователи - `admin@dataart.com / admin` `banana@dataart.com / banana`
