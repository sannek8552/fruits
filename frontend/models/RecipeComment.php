<?php

/**
 * This is the model class for table "{{recipe_comment}}".
 *
 * The followings are the available columns in table '{{recipe_comment}}':
 *
 * @property string $id
 * @property string $recipe_id
 * @property string $user_id
 * @property string $text
 * @property string $date
 *
 * The followings are the available model relations:
 * @property User   $user
 * @property Recipe $recipe
 */
class RecipeComment extends ActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{recipe_comment}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('recipe_id, text', 'required'),
            array('recipe_id, user_id', 'length', 'max' => 10),
            array('text, date', 'safe'),
            array('id, recipe_id, user_id, text, date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user'   => array(self::BELONGS_TO, 'User', 'user_id'),
            'recipe' => array(self::BELONGS_TO, 'Recipe', 'recipe_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'        => 'ID',
            'recipe_id' => 'Recipe',
            'user_id'   => 'User',
            'text'      => 'Text',
            'date'      => 'Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('recipe_id', $this->recipe_id, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('date', $this->date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return RecipeComment the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    protected function beforeSave()
    {
        //set user_id for new comments
        if ($this->isNewRecord) {
            $this->user_id = Yii::app()->user->id;
        }

        $this->date = new CDbExpression('NOW()');

        return parent::beforeSave();
    }

    protected function afterSave()
    {
        Logger::info(sprintf("Recipe comment (ID: %d) created", $this->id), 'frontend.model.RecipeComment');
        parent::afterSave();
    }

    protected function afterDelete()
    {
        Logger::info(sprintf("Recipe comment (ID: %d) deleted", $this->id), 'frontend.model.RecipeComment');
        parent::afterDelete();
    }


}
