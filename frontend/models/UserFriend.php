<?php

/**
 * Class UserFriend
 *
 * @property $user_id
 * @property $friend_id
 */
class UserFriend extends ActiveRecord
{
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user_friends}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('user_id, friend_id', 'required'),
		);
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @param int $userId
     * @param $friendId
     *
     * @return int CActiveRecord[]
     */
    public function loadFriends($userId, $friendId)
    {
        return  $this->findAllBySql('SELECT * FROM {{user_friends}}
            WHERE (user_id = :userId AND friend_id = :friendId) OR (user_id = :friendId AND friend_id = :userId)',
            array(
                ':userId'   => $userId,
                ':friendId' => $friendId,
            ));
    }

}
