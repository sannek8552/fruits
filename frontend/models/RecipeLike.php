<?php

/**
 * This is the model class for table "{{recipe_like}}".
 *
 * The followings are the available columns in table '{{recipe_like}}':
 * @property string $user_id
 * @property string $recipe_id
 */
class RecipeLike extends ActiveRecord
{

    const LIKE_ADDED   = 1;
    const LIKE_DELETED = 2;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{recipe_like}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('user_id, recipe_id', 'length', 'max'=>10),
			array('user_id, recipe_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'recipe_id' => 'Recipe',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('recipe_id',$this->recipe_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RecipeLike the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Added or deleted recipe like
     *
     * @param int $userId
     * @param int $recipeId
     *
     * @return int|null
     * @throws Exception
     */
    public function like($userId, $recipeId)
    {
        $like = $this->findByPk(array('user_id' => $userId, 'recipe_id' => $recipeId));

        $result = null;
        $trans = $this->dbConnection->beginTransaction();
        try {

            if ($like) {
                $like->delete();
                $result = self::LIKE_DELETED;
                Logger::info(sprintf("User (ID: %d) like recipe (ID: %d)", $userId, $this->recipe_id), 'frontend.model.RecipeLike');
            } else {
                $like = new RecipeLike();
                $like->user_id = $userId;
                $like->recipe_id = $recipeId;
                $like->save();
                $result = self::LIKE_ADDED;
                Logger::info(sprintf("User (ID: %d) unlike recipe (ID: %d)", $userId, $this->recipe_id), 'frontend.model.RecipeLike');
            }
            $trans->commit();
        } catch (Exception $e) {
            $trans->rollback();
            throw $e;
        }

        return $result;
    }
}

