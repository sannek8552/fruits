<?php

/**
 * This is the model class for table "{{recipe}}".
 *
 * The followings are the available columns in table '{{recipe}}':
 * @property string $id
 * @property string $user_id
 * @property string $title
 * @property string $text
 * @property string $photo
 * @property string $date
 * @property User[] $userLikes
 *
 * The followings are the available model relations:
 * @property User $user
 * @property RecipeComment[] $comments
 */
class Recipe extends ActiveRecord
{
    const IMAGE_PATH = '/upload/recipes/';

    /**
     *
     * @return string
     */
    public static function getImagePath() {
        return '.' . self::IMAGE_PATH;
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{recipe}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('title, text', 'required'),
            array('title', 'match', 'pattern'=>'/^[\w\s,]+$/', 'message'=>'Title should have only letters'),
            array('photo', 'required', 'on' => 'create'),
			array('user_id', 'length', 'max'=>10),
			array('title', 'length', 'max'=>255),
			array('text, date', 'safe'),
			array('photo', 'file', 'allowEmpty' => true),
			array('id, user_id, title, text, photo, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'comments' => array(self::HAS_MANY, 'RecipeComment', 'recipe_id', 'index' => 'id', 'order'=>'date DESC'),
			'userLikes' => array(self::MANY_MANY, 'User', 'tbl_recipe_like(recipe_id, user_id)', 'index' => 'id', 'order'=>'id DESC'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'title' => 'Title',
			'text' => 'Text',
			'photo' => 'Photo',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Recipe the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function saveRecipe()
    {
        $fileInstance = CUploadedFile::getInstance($this, 'photo');
        if ($fileInstance) {
            $this->photo = Helpers::getUniqueFileName(self::getImagePath(), $fileInstance->name);
        }
        if (!$this->validate()) {
            return false;
        }

        $trans = $this->dbConnection->beginTransaction();
        try {
            if (parent::save(false)) {
                if ($fileInstance && !$fileInstance->saveAs(self::getImagePath() . $this->photo)) {
                    $this->addError('photo', 'Cannot upload file: '. $this->photo);
                    $trans->rollback();

                    return false;
                }
                $trans->commit();
                Logger::info(sprintf("Recipe (ID: %d) created", $this->id), 'frontend.model.Recipe');

                return true;
            }
        } catch (Exception $e) {
            $trans->rollback();
            Logger::error($e->getMessage(), 'frontend.model.Recipe');
            throw $e;
        }

        return false;
    }

    /**
     * @return bool
     */
    protected function beforeSave()
    {
        //set user_id for new recipes
        if ($this->isNewRecord) {
            $this->user_id = Yii::app()->user->id;
        }

        $this->date = new CDbExpression('NOW()');

        return parent::beforeSave();
    }

    protected function afterDelete()
    {
        Logger::info(sprintf("Recipe (ID: %d) deleted", $this->id), 'frontend.model.Recipe');
        parent::afterDelete();
    }


}
