<?php
/**
 * Created by PhpStorm.
 * User: alfedorov
 * Date: 17.06.14
 * Time: 12:52
 */
class test
{
    protected static $obj;

    public function count()
    {
        static $method;
        ++$method;
        ++self::$obj;
        return array('method'=>$method, 'obj'=>self::$obj);
    }
}

$t1 = new test();
var_dump($t1->count()); // ["method"]=>  int(1),  ["obj"]=>  int(1)

$t2 = new test();
var_dump($t2->count()); // ["method"]=>  int(2),  ["obj"]=>  int(1)
