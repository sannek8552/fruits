var LIKE_ADDED   = 1;
var LIKE_DELETED = 2;

$(function () {

    $('button.like, button.unlike').click(like);

    $('button.toggle-comments').click(loadComments);

    $('.comments')
        .on('click', '.post-comment', postComment)
        .on('click', '.delete-comment', deleteComment);

    $(document).on('click', '.delete-recipe', deleteRecipe);

    $('.likes a').one('mouseenter', function () {
        var $el = $(this), $tooltip = $el.find('.tooltip'), $cont = $tooltip.find('.tooltip-body');
        var params = {
            recipeId: $el.closest('.item').data('recipeId')
        };

        $cont.load(
            '/recipe/ajaxlikedusers',
            params,
            function () {

                $tooltip.fadeIn("slow");

                $tooltip.css({
                    top: -$tooltip.innerHeight(),
                    left: ($el.width() - $tooltip.width())/2
                });
            }
        );

        var timer;
        $el.hover(function () {
            clearTimeout(timer);
            timer = setTimeout(function () {
                $tooltip.fadeIn("slow");
            }, 200);
        }, function () {
            clearTimeout(timer);
            timer = setTimeout(function () {
                $tooltip.fadeOut("slow");
            }, 200);
        });
    });
});

/**
 * Post recipe comment
 *
 */
function postComment () {
    var $cont = $(this).closest('.comment-item');
    var $textarea = $cont.find('textarea'),
        comment = $.trim($textarea.val());
    if (!comment) {
        return;
    }

    var $btn  = $(this).button('loading');
    var params = {
        comment:  comment,
        recipeId:  $btn.data('recipeId')
    };

    setTimeout(function() {
        $.post(
            '/recipe/ajaxpostcomment',
            params,
            function (response) {
                if (response.content) {
                    $cont.before(response.content);
                }
                $textarea.val('');
                $btn.button('reset');
            },
            'json'
        );
    }, 1000);
}


function deleteComment () {

    if (!confirm('Are you sure?')) {
        return;
    }

    var $btn  = $(this).button('loading'),
        $cnt  = $btn.closest('.comment-item');
    var params = {
        id:  $cnt.data('commentId')
    };

    setTimeout(function() {
        $.post(
            '/recipe/ajaxdeletecomment',
            params,
            function () {
                $btn.button('reset');
                $cnt.slideUp(500, function () {$(this).remove()});
            },
            'json'
        );
    }, 1000);
}

function deleteRecipe () {

    if (!confirm('Are you sure?')) {
        return;
    }

    var $btn  = $(this).button('loading'),
        $cnt  = $btn.closest('.item');
    var params = {
        id:  $cnt.data('recipeId')
    };

    setTimeout(function() {
        $.post(
            '/recipe/ajaxdeleterecipe',
            params,
            function () {
                $btn.button('reset');
                $cnt.slideUp(500, function () {$(this).remove()});
            },
            'json'
        );
    }, 1000);
}

/**
 * Toggle comments block
 *
 */
function toggleComments () {
    var $cont = $(this).closest('.item').find('.comments');
    $cont.collapse('toggle');
}

/**
 * Load comments and show comments block
 */
function loadComments () {
    var $cont = $(this).closest('.item').find('.comments'),
        $btn  = $(this).button('loading');
        var params = {
            recipeId:  $btn.data('recipeId')
        };

    setTimeout(function() {
        $cont.load(
            '/recipe/ajaxcomments',
            params,
            function () {
                $btn.unbind('click', loadComments);
                $btn.button('reset')
                $btn.click(toggleComments);
                $btn.trigger('click');
            }
        );
    }, 1000);

}

/**
 * Like recipe
 */
function like() {
    var $button = $(this).button('loading');
    var data = $button.data();

    var params = {
        recipeId: data.recipeId
    };

    $.ajax({
        url: '/recipe/ajaxlike',
        dataType: 'json',
        type: 'POST',
        data: params,
        success: function (res) {
            setTimeout(function () {
                $button.button('reset').hide();
                $button.parent().find(res.result == LIKE_ADDED ? '.unlike' : '.like').show();
                $button.closest('.item').find('.likes-number').html(res.likesNumber);

                var $cont = $button.closest('.item').find('.likes');
                if (res.likesNumber > 0) {
                    $cont.show();
                } else {
                    $cont.hide();
                }
            }, 500);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

