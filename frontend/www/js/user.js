var LIKE_ADDED   = 1;
var LIKE_DELETED = 2;

$(function () {
    var scrollOptions = {
        data: {
            filter: $('.search-query').val()
        },
        offset: 5,
        limit: 5,
        url: '/user/ajaxuser',
        loadingHtml: 'Loading...',
        dataType: 'html',
        success: function (res) {
            $(this).append(res);
        }
    };
    $('.scrollable').scrollable(scrollOptions);

    //search user handler
    $('.user-search-panel').on('submit', function (e) {
        e.preventDefault();

        $('.scrollable').scrollable('destroy');
        var filter = $('.search-query').val();
        $('.user-list').load(
            '/user/ajaxuser',
            {
                filter: filter
            },
            function () {
                scrollOptions.data.filter = $('.search-query').val();
                scrollOptions.offset = 5;
                $('.scrollable').scrollable(scrollOptions);
            }
        );
    });

    //Add friend handler
    $(document).on('click', 'button.add-friend, button.remove-friend', function () {
        var $button = $(this).button('loading');
        var data = $button.data();

        var params = {
            isFriend: data.isFriend,
            friendId: data.friendId
        };

        $.ajax({
            url: '/user/ajaxfriend',
            dataType: 'json',
            type: 'POST',
            data: params,
            success: function () {
                setTimeout(function () {
                    $button.button('reset').hide();
                    $button.parent().find(params.isFriend ? '.remove-friend' : '.add-friend').show();
                }, 500);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    //Block/Activate User
    $(document).on('click', 'button.block-user', blockUser);
});


/**
 * Block/Activate User
 */
function blockUser() {
    var $button = $(this).button('loading');
    var userId = $button.closest('.item').data('userId');

    var params = {
        userId: userId
    };

    $.ajax({
        url: '/user/ajaxblock',
        dataType: 'json',
        type: 'POST',
        data: params,
        success: function (res) {
            setTimeout(function () {
                $button.button('reset').hide();
                $button.parent().find(res.isBlocked ? '.activate' : '.block').show();
            }, 500);
        },
        error: function (error) {
            console.log(error);
        }
    });
}



