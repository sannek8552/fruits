/**
 * Created by alfedorov on 27.06.14.
 */

// Create closure.
(function($) {

    $.fn.scrollable = function(method) {

        var SCROLL_OFFSET = 100;
        var $this = this;

        var defaults = {
            url: '',
            limit: 10,
            offset: 0,
            dataType: 'json',
            loadingHtml: 'Loading...',
            data: {},
            success: function (res) {},
            isLastQuery: function (res, limit) {
                return res.length < limit;
            }
        };



        var methods = {
            init : function(options) {
                return $this.each(function(){
                    var settings = $.extend(true, {}, defaults, options);

                    var $elem = $(this),
                        self = this,
                        isLoading = false,
                        offset = settings.offset,
                        isWindow = ($elem.css('overflow-y') === 'visible'),
                        $scroll = isWindow ? $(window) : $elem,
                        offsetTop = ($elem.length ? $elem.offset().top : 0);



                    var scrollHandler = function () {
                        var scrollBottom = isWindow
                            ? $elem.height() - $scroll.scrollTop() - $scroll.height() + offsetTop
                            : $elem.scrollHeight - $scroll.scrollTop() - $elem.height()  + offsetTop;

                        if (scrollBottom < SCROLL_OFFSET) {
                            if (!isLoading) {
                                isLoading = true;
                                $('<div id="loadingBlock"/>').html(settings.loadingHtml).appendTo($elem);

                                var params = $.extend({}, settings.data, {
                                    limit: settings.limit,
                                    offset: offset
                                });

                                $.ajax({
                                    dataType: settings.dataType,
                                    url: settings.url,
                                    data: params,
                                    success: function (res) {
                                        isLoading = false;
                                        $('#loadingBlock').remove();
                                        offset += settings.limit;
                                        settings.success.call(self, res);

                                        if (settings.isLastQuery.call(this, res, settings.limit)) {
                                            $scroll.off('.scrollable');
                                        }

                                    },
                                    error: function () {
                                        isLoading = false;
                                    }
                                });
                            }
                        }
                    };

                    $scroll.on('scroll.scrollable', scrollHandler);
                });

            },
            destroy : function( ) {
                return $this.each(function(){
                    var isWindow = ($(this).css('overflow-y') === 'visible'),
                        $scroll = isWindow ? $(window) : $(this);
                    $scroll.off('.scrollable');
                });
            }
        };

        if ( methods[method] ) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || ! method) {
            return methods.init.apply( this, arguments );
        } else {
            $.error('Method ' +  method + ' is not exist' );
        }
    };
})(jQuery);
