<?php

class UserController extends BaseFrontendController
{
    public function init()
    {
        parent::init();

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl.'/js/libs/jquery.scrollable.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/user.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/recipe.js', CClientScript::POS_END);
    }

    public function actionList()
    {
        $users = User::model()->findAll(array('limit' => 5));

        $this->render('list', array(
            'users' => $users,
            'offset' => 0,
            'filter' => ''
        ));
    }

    public function actionAjaxUser()
    {
        $offset = $this->getActionParam('offset', 0);
        $limit = $this->getActionParam('limit', 5);
        $filter = $this->getActionParam('filter', '');

        $criteria = new CDbCriteria;
        $criteria->limit = $limit;
        $criteria->offset = (int)$offset;

        if ($filter) {
            //$criteria->condition = 'name LIKE "%:param%"';
            $criteria->compare('name', $filter, true);
        }

        $users = User::model()->findAll($criteria);

        echo $this->renderPartial('include/user-list', array(
            'users' => $users,
            'offset' => $offset,
            'filter' => $filter,
        ));
    }

    public function actionAjaxFriend()
    {
        $friendId = $this->getActionParam('friendId');
        if (!Yii::app()->user->isGuest && $friendId != Yii::app()->user->id) {
            $isFriend = $this->getActionParam('isFriend');
            $friendId = $this->getActionParam('friendId');
            $user = Yii::app()->user->getModel();
            try {
                $user->addFriend($friendId, $isFriend);
                $this->renderJson(array('success' => true));
            } catch (Exception $e) {
                $this->renderJsonError(array(
                    'success' => false,
                    'error' => $e->getMessage(),
                ));
            }
        }
    }

    public function actionAjaxBlock()
    {
        if (!$this->checkAccess(Permission::BLOCK_USER)) {
            throw new CHttpException(403);
        }
        $userId = $this->getActionParam('userId');

        if ($userId == Yii::app()->user->id) {
            throw new CHttpException(400, 'Cannot block yourself');
        }

        $isBlocked = User::model()->block($userId);

        $this->renderJson(array('isBlocked' => $isBlocked));
    }

    public function actionView()
	{
        $name = $this->getActionParam('name');
        /** @var User $user */
        $user = User::model()->find('LOWER(name) = ?', array(strtolower($name)));

        if (!$user) {
            throw new CHttpException(404);
        }

		$this->render('view', array(
            'user' => $user,
        ));
	}
}
