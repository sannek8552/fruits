<?php

class HomeController extends BaseFrontendController
{
	public function actionIndex()
	{
        $this->render('index');
	}

    public function actionLang()
    {
        $lang = $this->getActionParam('l');
        if ($lang) {
            Yii::app()->session['lang'] = $lang;
        }
        $this->redirect('/home');
    }

    public function actionTheme()
    {
        $theme = $this->getActionParam('theme');
        if ($theme) {
            /** @var \Config $config */
            $config = Config::model()->findByAttributes(array('name' => 'theme'));
            if ($config) {
                $config->value = $theme;
                $config->save();
            }
        }
        $this->redirect('/home');
    }

	public function filters()
	{
        return array(
            'accessControl',
        );
	}

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array('theme'),
                'roles'   => array('admin'),
            ),
            array(
                'deny',
                'actions' => array('theme'),
            ),
        );
    }
}
