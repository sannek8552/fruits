<?php

class ThumbsController extends EController
{
    public function actions()
    {
        return array(
            'avatar' => array(
                'class'   => 'resizer.ResizerAction',
                'options' => array(
                    // Tmp dir to store cached resized images
                    'cache_dir'   => Yii::getPathOfAlias('webroot') . '/assets/upload/',

                    // Web root dir to search images from
                    'base_dir'    => Yii::getPathOfAlias('webroot') . '/upload/',
                )
            ),
            'recipe' => array(
                'class'   => 'resizer.ResizerAction',
                'options' => array(
                    // Tmp dir to store cached resized images
                    'cache_dir'   => Yii::getPathOfAlias('webroot') . '/assets' . Recipe::IMAGE_PATH,

                    // Web root dir to search images from
                    'base_dir'    => Yii::getPathOfAlias('webroot') . Recipe::IMAGE_PATH,
                )
            ),
        );
    }
}
