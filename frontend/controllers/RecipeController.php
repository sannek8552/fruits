<?php

class RecipeController extends BaseFrontendController
{
    public function init()
    {
        parent::init();

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl.'/js/recipe.js', CClientScript::POS_END);
    }

	public function actionEdit($id = null)
	{
        if (!$this->checkAccess(Permission::EDIT_RECIPE)) {
            throw new CHttpException(403);
        }

        if ($id) {
            /** @var Recipe $recipe */
            $recipe = Recipe::model()->findByPk((int)$id);
            if (!$recipe) {
                throw new CHttpException(404, Yii::t('Common', 'Recipe not found'));
            }
            if (!Yii::app()->user->isAdmin() && $recipe->user_id != Yii::app()->user->id) {
                throw new CHttpException(403);
            }
        } else {
            $recipe = new Recipe();
        }

        $assign = array();
        if (Yii::app()->request->isPostRequest) {
            if (!$id) {
                $recipe->setScenario('create');
            }
            unset($_POST['Recipe']['photo']);
            $recipe->attributes = $_POST['Recipe'];

            if ($recipe->saveRecipe()) {
                $this->setSuccessMessage('Recipe saved!');
                $this->redirect('/recipe');
            }
        }

        $assign['recipe'] = $recipe;

		$this->render('edit', $assign);
	}

	public function actionIndex()
	{
        $recipes = Recipe::model()->findAll(array('order'=>'date DESC'));

		$this->render('index', array(
            'recipes' => $recipes
        ));
	}

	public function actionView()
	{
		$this->render('view');
	}

    public function actionAjaxLike()
    {
        if (!Yii::app()->user->isGuest) {
            $recipeId = $this->getActionParam('recipeId');
            $userId   = Yii::app()->user->id;
            try {
                $result = RecipeLike::model()->like($userId, $recipeId);
                $likesNumber = RecipeLike::model()->countByAttributes(array(
                    'recipe_id' => $recipeId
                ));
                $this->renderJson(array(
                    'success' => true,
                    'result' => $result,
                    'likesNumber' => $likesNumber,
                ));
            } catch (Exception $e) {
                $this->renderJsonError(array(
                        'success' => false,
                        'error' => $e->getMessage(),
                    ));
            }
        }
    }

    public function actionAjaxComments()
    {
        $recipeId = $this->getActionParam('recipeId');
        $comments = RecipeComment::model()->findAllByAttributes(array('recipe_id' => $recipeId));

        $this->renderPartial('include/recipe-comments', array(
                'comments' => $comments,
                'recipeId' => $recipeId,
            ));
    }

    public function actionAjaxLikedUsers()
    {
        $recipeId = $this->getActionParam('recipeId');
        /** @var Recipe $recipe */
        $recipe = Recipe::model()->findByPk($recipeId);

        if ($recipe) {
            $this->renderPartial('include/recipe-liked-users', array(
                    'users' => $recipe->userLikes,
                ));
        }
    }

    public function actionAjaxPostComment()
    {
        $comment = $this->getActionParam('comment');
        $recipeId = $this->getActionParam('recipeId');

        $model = new RecipeComment();
        $model->text = $comment;
        $model->recipe_id = $recipeId;

        $result = array(
            'success' => false
        );

        if ($model->save()) {
            $result['content'] = $this->renderPartial('include/recipe-comment', array(
                    'comment' => RecipeComment::model()->findByPk($model->id),
                ), true);
            $result['success'] = true;
        }

        $this->renderJson($result);
    }

    public function actionAjaxDeleteRecipe()
    {
        if (!$this->checkAccess(Permission::DELETE_RECIPE)) {
            throw new CHttpException(403);
        }

        $id = $this->getActionParam('id');

        $recipe = Recipe::model()->findByPk((int)$id);
        if (!$recipe) {
            throw new CHttpException(404, Yii::t('Common', 'Recipe not found'));
        }
        $recipe->delete();

        $this->renderJson(array('success' => true));
    }

    public function actionAjaxDeleteComment()
    {
        if (!$this->checkAccess(Permission::DELETE_COMMENT)) {
            throw new CHttpException(403);
        }

        $id = $this->getActionParam('id');

        $comment = RecipeComment::model()->findByPk((int)$id);
        if (!$comment) {
            throw new CHttpException(404, Yii::t('Common', 'Comment not found'));
        }
        $comment->delete();

        $this->renderJson(array('success' => true));
    }
}
