<?php
/**
 * @var $this AuthController
 * @var $user User
 * */

$this->breadcrumbs=array(
    'Auth'=>array('/auth'),
    'Login',
);
?>
<h1>Login</h1>
<?php if ($user->hasErrors()) { ?>
    <div class="alert alert-error">
        <?php echo CHtml::errorSummary($user, false); ?>
    </div>
<?php } ?>

<form action="/auth/login" method="post" class="form-horizontal">
    <div class="control-group <?php if ($user->getError('email')) { ?>error<?php } ?>">
        <label class="control-label" for="signup-email"><?php echo Yii::t('User', 'Email'); ?></label>
        <div class="controls">
            <input type="text" name="form[email]" id="signup-email" value="<?php echo $user->email; ?>" />
        </div>
    </div>
    <div class="control-group <?php if ($user->getError('password')) { ?>error<?php } ?>">
        <label class="control-label" for="signup-password"><?php echo Yii::t('User', 'Password'); ?></label>
        <div class="controls">
            <input type="password" name="form[password]" id="signup-password" />
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button class="btn btn-primary"><?php echo Yii::t('Common', 'Login'); ?></button>
        </div>
    </div>
</form>
