<?php
/**
 * @var $this AuthController
 * @var $user User
 * */

$this->breadcrumbs=array(
	'Auth'=>array('/auth'),
	'Signup',
);
?>
<h1><?php echo Yii::t('Common', 'Registration'); ?></h1>

<?php if ($user->hasErrors()) { ?>
    <div class="alert alert-error">
        <?php echo CHtml::errorSummary($user, false); ?>
    </div>
<?php } ?>

<form action="/auth/signup" method="post" class="form-horizontal" enctype="multipart/form-data">
    <div class="control-group <?php if ($user->getError('name')) { ?>error<?php } ?>">
        <label class="control-label" for="signup-name"><?php echo Yii::t('User', 'Name'); ?></label>
        <div class="controls">
            <?php echo CHtml::activeTextField($user, 'name'); ?>
        </div>
    </div>

    <div class="control-group <?php if ($user->getError('email')) { ?>error<?php } ?>">
        <label class="control-label" for="signup-email"><?php echo Yii::t('User', 'Email'); ?></label>
        <div class="controls">
            <?php echo CHtml::activeTextField($user, 'email'); ?>
<!--            <input type="text" name="email" id="signup-email" value="--><?php //echo $user->email; ?><!--" />-->
        </div>
    </div>
    <div class="control-group <?php if ($user->getError('password')) { ?>error<?php } ?>">
        <label class="control-label" for="signup-password"><?php echo Yii::t('User', 'Password'); ?></label>
        <div class="controls">
            <?php echo CHtml::activePasswordField($user, 'password'); ?>
<!--            <input type="password" name="password" id="signup-password" />-->
        </div>
    </div>
    <div class="control-group <?php if ($user->getError('cpassword')) { ?>error<?php } ?>">
        <label class="control-label" for="signup-cpassword"><?php echo Yii::t('User', 'Confirm password'); ?></label>
        <div class="controls">
            <?php echo CHtml::activePasswordField($user, 'cpassword'); ?>
<!--            <input type="password" name="cpassword" id="signup-cpassword" />-->
        </div>
    </div>
    <div class="control-group <?php if ($user->getError('photo')) { ?>error<?php } ?>">
        <label class="control-label" for="signup-photo"><?php echo Yii::t('User', 'Photo'); ?></label>
        <div class="controls">
            <?php echo CHtml::activeFileField($user, 'photo'); ?>
<!--            <input type="file" name="photo" id="signup-photo" />-->
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button class="btn btn-primary"><?php echo Yii::t('Common', 'Sign up'); ?></button>
        </div>
    </div>
</form>
