<?php
/**
 * @var $recipe Recipe
 */
?>
<div class="item" data-recipe-id="<?php echo CHtml::encode($recipe->id) ?>">
    <div class="avatar">
        <img src="/thumbs/recipe/120x120/<?php echo CHtml::encode($recipe->photo) ?>"
             alt="<?php echo Yii::t('Recipe', 'Recipe photo'); ?>"/>
    </div>
    <div class="info">
        <div class="body">
            <a class="title" href="javascript:void(0);"><?php echo CHtml::encode($recipe->title) ?></a>

            <p class="nbm"><?php echo CHtml::encode($recipe->text) ?></p>

            <div class="controls full-width">
                <div class="buttons pull-left">
                    <?php if (!Yii::app()->user->isGuest) { ?>
                        <?php $liked = isset($recipe->userLikes[Yii::app()->user->id]); ?>
                        <button class="btn btn-mini btn-primary like <?php if ($liked) { ?>hide<?php } ?>"
                                data-recipe-id="<?php echo CHtml::encode($recipe->id) ?>">
                            <i class="icon-thumbs-up"></i> &nbsp;<?php echo Yii::t('Common', 'Like'); ?>
                        </button>
                        <button class="btn btn-mini btn-success unlike <?php if (!$liked) { ?>hide<?php } ?>"
                                data-recipe-id="<?php echo CHtml::encode($recipe->id) ?>">
                            <i class="icon-ok"></i> &nbsp;<?php echo Yii::t('Common', 'Like'); ?>
                        </button>

                        <button class="btn btn-mini btn-primary toggle-comments" data-recipe-id="<?php echo CHtml::encode($recipe->id) ?>">
                            <i class="icon-comment"></i> &nbsp;<?php echo Yii::t('Common', 'Comments'); ?>
                        </button>
                    <?php } ?>
                    <?php if (Yii::app()->user->checkAccess(Permission::EDIT_RECIPE) &&
                        (Yii::app()->user->isAdmin() || $recipe->user_id == Yii::app()->user->id)) { ?>
                        <a href="<?php echo Yii::app()->createUrl('/recipe/edit', array('id' => $recipe->id)) ?>" class="btn btn-mini btn-primary edit-recipe">
                            <i class="icon-pencil"></i> &nbsp;<?php echo Yii::t('Common', 'Edit'); ?>
                        </a>
                    <?php } ?>
                    <?php if (Yii::app()->user->checkAccess(Permission::DELETE_RECIPE)) { ?>
                        <button class="btn btn-mini btn-danger delete-recipe">
                            <i class="icon-trash"></i> &nbsp;<?php echo Yii::t('Common', 'Delete'); ?>
                        </button>
                    <?php } ?>
                </div>
                <div class="likes pull-left <?php if (!$recipe->userLikes) { ?>hide<?php } ?>">
                    <a href="javascript:void(0);">
                        <span class="likes-number"><?php echo count($recipe->userLikes) ?></span> like(s)
                        <div class="tooltip top tooltip-likes">
                            <div class="tooltip-arrow"></div>
                            <div class="tooltip-inner">
                                <div class="tooltip-body"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="date pull-right">
                    <span class="muted">posted <?php echo strtolower(Helpers::dateToAgoFormat($recipe->date)); ?> by</span>
                    <a href="/u/<?php echo $recipe->user->name ?>"><?php echo $recipe->user->name ?></a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="collapse comments"></div>
    </div>
</div>
