<?php
/**
 * @var $comments RecipeComment[]
 * @var $recipeId int
 */
?>

<?php foreach ($comments as $comment) {
    echo $this->renderPartial('include/recipe-comment', array(
            'comment'  => $comment,
        ));
} ?>
<div class="comment-item">
    <div class="comment-avatar">
        <img src="/thumbs/avatar/80x80/<?php echo CHtml::encode(Yii::app()->user->getModel()->photo) ?>"
             alt="<?php echo Yii::t('Recipe', 'Author avatar'); ?>"/>
    </div>
    <div class="comment-info">
        <textarea class="comment-text full-width"></textarea>
        <button class="btn btn-primary post-comment" data-recipe-id="<?php echo CHtml::encode($recipeId) ?>">
            <i class="icon-ok"></i> <?php echo Yii::t('Common', 'Add'); ?>
        </button>
    </div>
    <div class="clearfix"></div>
</div>
