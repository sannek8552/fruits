<?php
/**
 * @var $comment RecipeComment
 */
?>
<div class="comment-item" data-comment-id="<?php echo CHtml::encode($comment->id) ?>">
    <div class="comment-avatar">
        <img src="/thumbs/avatar/80x80/<?php echo CHtml::encode($comment->user->photo) ?>"
             alt="<?php echo Yii::t('Recipe', 'Author avatar'); ?>"/>
    </div>
    <div class="comment-info">
        <a href="/u/<?php echo CHtml::encode($comment->user->name) ?>"><?php echo CHtml::encode($comment->user->name) ?></a>
        <p class="nbm"><?php echo CHtml::encode($comment->text) ?></p>
        <div class="controls full-width">
            <div class="muted pull-left"><?php echo Helpers::dateToAgoFormat($comment->date); ?></div>
            <?php if (Yii::app()->user->checkAccess(Permission::DELETE_COMMENT)) { ?>
                <button class="btn btn-mini btn-danger delete-comment pull-right"><i class="icon-trash"></i> &nbsp;<?php echo Yii::t('Common', 'Delete'); ?></button>
            <?php } ?>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
