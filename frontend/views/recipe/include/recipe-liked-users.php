<?php
/**
 * @var $users User[]
 */
?>
<p>Was interesting for <?php echo CHtml::encode(count($users)) ?> people</p>
<?php foreach (array_slice($users, 0, 5) as $user) { ?>
    <a href="/u/<?php echo CHtml::encode($user->name) ?>" class="pull-left">
        <img style="width: 30px; height: 30px;" src="/thumbs/avatar/30x30/<?php echo CHtml::encode($user->photo) ?>" alt="User avatar"/>
    </a>
<?php } ?>
<div class="clearfix"></div>
