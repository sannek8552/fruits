<?php
/**
 * @var $this RecipeController
 * @var $recipes Recipe[]
 */

$this->breadcrumbs=array(
    Yii::t('Common', 'Recipe'),
);
?>
<h1><?php echo Yii::t('Recipe', 'Recipes'); ?></h1>
<div class="recipes">
    <?php foreach ($recipes as $recipe) {
        echo $this->renderPartial('include/recipe-item', array(
                'recipe'  => $recipe,
            ));
    } ?>
</div>

