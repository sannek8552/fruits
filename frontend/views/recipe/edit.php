<?php
/**
 * @var $this RecipeController
 * @var $recipe Recipe
 */

$this->breadcrumbs=array(
    Yii::t('Common', 'Recipe')=>array('/recipe'),
    Yii::t('Common', 'Edit'),
);
?>
<h1><?php echo Yii::t('Recipe', $recipe->id ? 'Edit Recipe' : 'Create new recipe'); ?></h1>

<?php if ($recipe->hasErrors()) { ?>
    <div class="alert alert-error">
        <?php echo CHtml::errorSummary($recipe, false); ?>
    </div>
<?php } ?>

<form action="<?php echo $this->createUrl('/recipe/edit', $recipe->id ? array('id' => $recipe->id) : array()); ?>" method="post" class="form-horizontal" enctype="multipart/form-data">

    <div class="control-group <?php if ($recipe->getError('title')) { ?>error<?php } ?>">
        <?php echo CHtml::activeLabel($recipe, 'title', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo CHtml::activeTextField($recipe, 'title', array('class' => 'input-xlarge')); ?>
        </div>
    </div>

    <div class="control-group <?php if ($recipe->getError('text')) { ?>error<?php } ?>">
        <?php echo CHtml::activeLabel($recipe, 'text', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo CHtml::activeTextArea($recipe, 'text', array('class' => 'full-width', 'rows' => 10)); ?>
        </div>
    </div>

    <div class="control-group <?php if ($recipe->getError('photo')) { ?>error<?php } ?>">
        <?php echo CHtml::activeLabel($recipe, 'photo', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo CHtml::activeFileField($recipe, 'photo'); ?>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button class="btn btn-primary"><?php echo Yii::t('Common', $recipe->id ? 'Save' : 'Create'); ?></button>
        </div>
    </div>
</form>
