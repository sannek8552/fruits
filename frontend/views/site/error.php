<div class="error-msg">
    <?php if ($message) {
        echo CHtml::encode($message);
    } else {
        switch ($code) {
            case 403:
                echo '403 - Access denied';
                break;
            case 404:
            default:
                echo '404 - Page Not Found';
                break;
        }
    } ?>
</div>

