<?php
/**
 * @var $this UserController
 * @var $users User[]
 * @var $offset string
 * @var $filter string
 */

$this->breadcrumbs=array(
    Yii::t('Common', 'Users')
);
?>

<h1><?php echo Yii::t('Common', 'User list'); ?></h1>
<form class="user-search-panel controls-row">
        <input type="text" class="search-query span10" placeholder="<?php echo Yii::t('Common', 'Search'); ?>">
        <button class="btn search-user span2"><?php echo Yii::t('Common', 'Search'); ?></button>
</form>
<div class="user-list scrollable">
    <?php echo $this->renderPartial('include/user-list', array(
            'users'  => $users,
            'offset' => $offset,
            'filter' => $filter
        )); ?>
</div>


