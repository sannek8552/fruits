<?php
/**
 * @var $this UserController
 * @var $user User
 * @var $recipes Recipe[]
 */

$this->breadcrumbs=array(
    Yii::t('Common', 'Users') => array('/users'),
	$user->name
);
?>

<h1><?php echo Yii::t('User', 'User profile'); ?></h1>

<div class="user-info">
    <div class="avatar">
        <img src="/thumbs/avatar/150x150/<?php echo CHtml::encode($user->photo) ?>" alt="<?php echo Yii::t('User', 'User photo'); ?>"/>
    </div>
    <h2 class="nbm"><?php echo CHtml::encode($user->name) ?></h2>
    <p><?php echo CHtml::encode($user->email) ?></p>
    <div class="controls">

    </div>

    <div class="clearfix"></div>
</div>

<div class="user-recipes recipes well">
    <fieldset>
        <legend>User Recipes</legend>
            <?php foreach ($user->recipes as $recipe) {
                echo $this->renderPartial('../recipe/include/recipe-item', array(
                        'recipe'  => $recipe,
                    ));
            } ?>

            <?php if (!$user->recipes) echo Yii::t('Common', 'No data found')?>
    </fieldset>
</div>



