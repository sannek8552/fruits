<?php
/**
 * @var $this UserController
 * @var $users User[]
 * @var $offset int
 * @var $filter string
 */
foreach ($users as $user) {
    //if ($user->id == Yii::app()->user->id) continue;
    ?>
    <div class="item" data-user-id="<?php echo CHtml::encode($user->id) ?>">
        <div class="avatar">
            <img src="/thumbs/avatar/120x120/<?php echo CHtml::encode($user->photo) ?>" alt="<?php echo Yii::t('User', 'User photo'); ?>"/>
        </div>
        <div class="info">
            <a class="user-title" href="/u/<?php echo urlencode(CHtml::encode($user->name)) ?>"><?php echo CHtml::encode($user->name) ?></a>
            <p><?php echo CHtml::encode($user->email) ?></p>
            <div class="controls">
                <?php if (!Yii::app()->user->isGuest && $user->id != Yii::app()->user->id) { ?>
                    <?php $alreadyFriend = isset(Yii::app()->user->getModel()->friends[$user->id]); ?>
                    <button class="btn btn-mini btn-primary remove-friend <?php if (!$alreadyFriend) { ?>hide<?php } ?>" data-friend-id="<?php echo CHtml::encode($user->id) ?>" data-is-friend="0">
                        <i class="icon-minus"></i> &nbsp;<?php echo Yii::t('Common', 'Remove from friends'); ?>
                    </button>
                    <button class="btn btn-mini btn-primary add-friend <?php if ($alreadyFriend) { ?>hide<?php } ?>" data-friend-id="<?php echo CHtml::encode($user->id) ?>" data-is-friend="1">
                        <i class="icon-plus"></i> &nbsp;<?php echo Yii::t('Common', 'Add to friends'); ?>
                    </button>
                <?php } ?>
                <a href="/u/<?php echo CHtml::encode($user->name) ?>" class="btn btn-mini btn-primary"><i class="icon-eye-open"></i> &nbsp;<?php echo Yii::t('Common', 'View Profile'); ?></a>
                <?php if (Yii::app()->user->checkAccess(Permission::BLOCK_USER) && !$user->hasRole('admin') && !$user->hasRole('moderator')) { ?>
                    <?php $isBlocked = $user->hasRole('blocked'); ?>
                    <button class="btn btn-mini btn-success block-user activate <?php if (!$isBlocked) { ?>hide<?php } ?>">
                        <i class="icon-ok"></i> &nbsp;<?php echo Yii::t('Common', 'Activate'); ?>
                    </button>
                    <button class="btn btn-mini btn-danger block-user block <?php if ($isBlocked) { ?>hide<?php } ?>">
                        <i class="icon-ban-circle"></i> &nbsp;<?php echo Yii::t('Common', 'Block'); ?>
                    </button>
                <?php } ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
<?php } ?>
