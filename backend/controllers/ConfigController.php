<?php

class ConfigController extends BaseBackendController
{
	public function actionIndex($id = null)
	{
        if ($id) {
            $config = Config::model()->findByPk($id);
            if (!$config) {
                throw new CHttpException(404, Yii::t('Common', 'Config not found'));
            }
        } else {
            $config = new Config();
        }

        if (Yii::app()->request->isPostRequest && isset($_POST['Config'])) {
            $config->attributes = $_POST['Config'];
            if ($config->save()) {
                $this->setSuccessMessage('Config saved!');
                $this->redirect('/config');
            }
        }

        /** @var Config $configModel */
        $configModel = Config::model();

        $criteria = new CDbCriteria(array(
            'order' => 'id desc',
        ));
        $count = $configModel->count($criteria);

        $pages = new CPagination($count);
        $pages->setPageSize(10);
        $pages->applyLimit($criteria);

        $configs = $configModel->findAll($criteria);

        $this->render(
            'index', array(
                'configs' => $configs,
                'pages'   => $pages,
                'config'  => $config,
            )
        );
	}

    public function actionDelete($id)
    {
        $config = Config::model()->findByPk($id);
        if (!$config) {
            throw new CHttpException(404, Yii::t('Common', 'Config not found'));
        }
        $config->delete();

        $this->redirect('/config');
    }
}
