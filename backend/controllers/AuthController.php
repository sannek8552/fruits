<?php

class AuthController extends BaseBackendController
{
    public function actionIndex()
    {
        $this->redirect('/auth/login');
    }

    public function actions()
    {
        return array(
            'login' => 'common.components.controllers.auth.LoginAction',
            'logout' => 'common.components.controllers.auth.LogoutAction',
            'signup' => 'common.components.controllers.auth.SignupAction',
        );
    }

    public function filters()
    {
        return array(

        );
    }
}
