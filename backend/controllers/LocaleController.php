<?php

class LocaleController extends BaseBackendController
{
	public function actionIndex($lang = null)
	{
        $language = new Language();
        if (Yii::app()->request->isPostRequest) {
            // save translations
            if (!empty($_POST['messages']) && !empty($_POST['lang'])) {
                foreach ($_POST['messages'] as $msg) {
                    /** @var SourceMessage $sourceMessage */
                    $sourceMessage = SourceMessage::model()->findByPk($msg['id']);
                    if ($sourceMessage) {
                        $sourceMessage->attributes = $msg;

                    } else {
                        $sourceMessage = new SourceMessage();
                        $sourceMessage->message = $msg['message'];
                        $sourceMessage->category = $msg['category'];
                    }
                    $sourceMessage->save();

                    /** @var Message $translation */
                    $translation = Message::model()->findByPk(array('id' => $sourceMessage->id, 'language' => $_POST['lang']));
                    if ($translation) {
                        $translation->attributes = $msg;
                    } else {
                        $translation = new Message();
                        $translation->id = $sourceMessage->id;
                        $translation->translation = $msg['translation'];
                        $translation->language = $_POST['lang'];
                    }
                    $translation->save();
                }

                $this->redirect('/locale/lang/' . $_POST['lang']);
            }

            //save new lang
            if (!empty($_POST['Language'])) {
                $language = new Language();
                $language->attributes = $_POST['Language'];
                $language->save();
            }
        }

        $translations = $messages = array();
        if ($lang) {
            $messages = SourceMessage::model()->with(
                array(
                    'translations' => array(
                        'on'       => 'language=:lang',
                        'joinType' => 'LEFT JOIN',
                        'params'   => array(':lang' => $lang)
                    ),
                )
            )->findAll();
            $translations = Message::model()->with('sourceMessage')->findAllByAttributes(array('language' => $lang), array('index' => 'id'));
        }

        $languages = Language::model()->findAll();
        $assign = array(
            'languages'    => $languages,
            'translations' => $translations,
            'messages'     => $messages,
            'lang'         => $lang,
            'language'     => $language,
        );
		$this->render('index', $assign);
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
