<?php

class LogController extends BaseBackendController
{
	public function actionIndex()
	{
        /** @var Log $logModel */
        $logModel = Log::model();

        $criteria = new CDbCriteria(array(
            'order' => 'id desc',
        ));
        $count = $logModel->count($criteria);

        $pages = new CPagination($count);
        $pages->setPageSize(10);
        $pages->applyLimit($criteria);

        $logs = $logModel->findAll($criteria);

		$this->render('index', array(
                'logs' => $logs,
                'pages' => $pages
            ));

	}

	public function actionView()
	{
		$this->render('view');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
