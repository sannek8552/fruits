<?php

class UserController extends BaseBackendController
{
    public function init()
    {
        parent::init();

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl.'/js/libs/jquery.scrollable.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/user.js', CClientScript::POS_END);
    }

    public function actionList()
    {
        $users = User::model()->findAll(array('limit' => 10));

        $this->render('list', array(
            'users' => $users,
            'offset' => 0,
            'filter' => ''
        ));
    }

    public function actionAjaxUser()
    {
        $offset = $this->getActionParam('offset', 0);
        $filter = $this->getActionParam('filter', '');
        $criteria = new CDbCriteria;
        $criteria->limit = 10;
        $criteria->offset = (int)$offset;

        if ($filter) {
            $criteria->compare('name', $filter, true);
        }

        $users = User::model()->findAll($criteria);

        echo $this->renderPartial('include/user-list', array(
            'users' => $users,
            'offset' => $offset,
            'filter' => $filter,
        ));
    }

    public function actionView()
	{
        $name = $this->getActionParam('name');
        /** @var User $user */
        $user = User::model()->find('LOWER(name) = ?', array(strtolower($name)));

        if (!$user) {
            throw new CHttpException(404);
        }

		$this->render('view', array(
            'user' => $user,
        ));
	}

    public function actionAjaxDeleteUser()
    {
        if (!$this->checkAccess(Permission::DELETE_USER)) {
            throw new CHttpException(403);
        }

        $id = $this->getActionParam('id');

        $user = User::model()->findByPk((int)$id);
        if (!$user) {
            throw new CHttpException(404, Yii::t('Common', 'User not found'));
        }
        $user->delete();

        $this->renderJson(array('success' => true));
    }
}
