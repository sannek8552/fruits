<?php

class ThumbsController extends BaseBackendController
{
    public function actions()
    {
        return array(
            'avatar' => array(
                'class'   => 'resizer.ResizerAction',
                'options' => array(
                    // Tmp dir to store cached resized images
                    'cache_dir'   => Yii::getPathOfAlias('frontend') . '/www/assets/upload/',

                    // Web root dir to search images from
                    'base_dir'    => Yii::getPathOfAlias('frontend') . '/www/upload/',
                )
            ),
        );
    }

    public function filters(){
        return array();
    }
}
