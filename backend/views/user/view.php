<?php
/**
 * @var $this UserController
 * @var $user User
 * @var $recipes Recipe[]
 */

$this->breadcrumbs=array(
	'Users' => array('/users'),
	$user->name
);
?>

<h1><?php echo Yii::t('User', 'User profile'); ?></h1>

<div class="user-info">
    <div class="avatar">
        <img src="/thumbs/avatar/150x150/<?php echo CHtml::encode($user->photo) ?>" alt="<?php echo Yii::t('User', 'User photo'); ?>"/>
    </div>
    <h2 class="nbm"><?php echo CHtml::encode($user->name) ?></h2>
    <p><?php echo CHtml::encode($user->email) ?></p>
    <div class="controls">
<!--        <?php /*if (Yii::app()->user->checkAccess(Permission::DELETE_USER) && !$user->hasRole('admin')) { */?>
            <button class="btn btn-danger delete-user">
                <i class="icon-trash"></i> <?php /*echo Yii::t('Common', 'Delete'); */?>
            </button>
        --><?php /*} */?>
    </div>

    <div class="clearfix"></div>
</div>




