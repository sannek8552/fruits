<?php
/**
 * @var $this UserController
 * @var $users User[]
 * @var $offset int
 * @var $filter string
 */
foreach ($users as $user) {
    if ($user->id == Yii::app()->user->id) continue;
    ?>
    <div class="item" data-user-id="<?php echo CHtml::encode($user->id) ?>">
        <div class="avatar">
            <img src="/thumbs/avatar/120x120/<?php echo CHtml::encode($user->photo) ?>" alt="<?php echo Yii::t('User', 'User photo'); ?>"/>
        </div>
        <div class="info">
            <a class="user-title" href="/u/<?php echo CHtml::encode($user->name) ?>"><?php echo CHtml::encode($user->name) ?></a>
            <p><?php echo CHtml::encode($user->email) ?></p>
            <div class="controls">
                <a href="/u/<?php echo CHtml::encode($user->name) ?>" class="btn btn-mini btn-primary"><i class="icon-eye-open"></i> &nbsp;<?php echo Yii::t('Common', 'View Profile'); ?></a>
                <?php if (Yii::app()->user->checkAccess(Permission::DELETE_USER) && !$user->hasRole('admin')) { ?>
                    <button class="btn btn-mini btn-danger delete-user">
                        <i class="icon-trash"></i> &nbsp;<?php echo Yii::t('Common', 'Delete'); ?>
                    </button>
                <?php } ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
<?php } ?>
