<?php
/**
 * @var $this LocaleController
 * @var $languages Language[]
 * @var $language Language
 * @var $messages SourceMessage[]
 * @var $lang string
 */

$this->breadcrumbs=array(
	'Locale',
);
?>
<h1><?php echo Yii::t('Common', 'Locale settings'); ?></h1>


    <div class="form-inline well">
        <div>
            <select name="languages" id="" onchange="window.location = '/locale/lang/'+this.value">
                <option value=""><?php echo Yii::t('Common', 'Select language'); ?></option>
                <?php foreach ($languages as $l) { ?>
                    <option value="<?php echo CHtml::encode($l->language) ?>" <?php if ($lang == $l->language) echo 'selected' ?>>
                        <?php echo CHtml::encode($l->language) ?></option>
                <?php } ?>
            </select>
            <button class="btn btn-primary" data-toggle="collapse" data-target="#lang-form"><i class="icon-plus"></i> <?php echo Yii::t('Common', 'Add new language'); ?></button>

        </div>
        <form  style="margin: 0;" id="lang-form" method="post" class="form-horizontal collapse <?php if ($language->hasErrors()) { ?>in<?php } ?>">
            <div style="margin-top: 20px;">
                <?php if ($language->hasErrors()) { ?>
                    <div class="alert alert-error">
                        <?php echo CHtml::errorSummary($language, false); ?>
                    </div>
                <?php } ?>
                <div class="control-group">
                    <label class="control-label"><?php echo Yii::t('Common', 'Language code'); ?></label>
                    <div class="controls">
                        <?php echo CHtml::activeTextField($language, 'language'); ?>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo Yii::t('Common', 'Language description'); ?></label>
                    <div class="controls">
                        <?php echo CHtml::activeTextField($language, 'language_str'); ?>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <button class="btn btn-success"><?php echo Yii::t('Common', 'Add'); ?></button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <?php if ($lang) { ?>
        <form method="post" action="/locale/lang/<?php echo CHtml::encode($lang) ?>">
            <input type="hidden" name="lang" value="<?php echo CHtml::encode($lang) ?>"/>
            <div class="row-fluid">
                <span class="span2"><?php echo Yii::t('Common', 'Category'); ?></span>
                <span class="span5"><?php echo Yii::t('Common', 'Source Message'); ?></span>
                <span class="span5"><?php echo Yii::t('Common', 'Translation'); ?></span>
            </div>
            <?php foreach ($messages as $msg) { ?>
                <div class="controls-row">
                    <input name="messages[<?php echo CHtml::encode($msg->id) ?>][id]" type="hidden" value="<?php echo CHtml::encode($msg->id) ?>"/>
                    <input name="messages[<?php echo CHtml::encode($msg->id) ?>][category]" class="span2" type="text" value="<?php echo CHtml::encode($msg->category) ?>"/>
                    <input name="messages[<?php echo CHtml::encode($msg->id) ?>][message]" class="span5" type="text" value="<?php echo CHtml::encode($msg->message) ?>"/>
                    <input name="messages[<?php echo CHtml::encode($msg->id) ?>][translation]" class="span5" type="text" value="<?php echo isset($msg->translations[0]) ? CHtml::encode($msg->translations[0]->translation) : '' ?>"/>
                </div>
            <?php } ?>
            <button class="btn btn-success" type="submit"><?php echo Yii::t('Common', 'Save'); ?></button>
        </form>
    <?php } ?>

