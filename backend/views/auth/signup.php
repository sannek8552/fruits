<?php
/**
 * @var $this AuthController
 * @var $user User
 * */

$this->breadcrumbs=array(
	'Auth' => array('/auth'),
	'Signup',
);
?>
<h1><?php echo Yii::t('Common', 'Registration'); ?></h1>

<?php if ($user->hasErrors()) { ?>
    <div class="alert alert-error">
        <?php echo CHtml::errorSummary($user, false); ?>
    </div>
<?php } ?>

<form action="/auth/signup" method="post" class="form-horizontal" enctype="multipart/form-data">
    <div class="control-group <?php if ($user->getError('name')) { ?>error<?php } ?>">
        <label class="control-label" for="signup-name"><?php echo Yii::t('User', 'Name'); ?></label>
        <div class="controls">
            <?php echo CHtml::activeTextField($user, 'name'); ?>
        </div>
    </div>

    <div class="control-group <?php if ($user->getError('email')) { ?>error<?php } ?>">
        <label class="control-label" for="signup-email"><?php echo Yii::t('User', 'Email'); ?></label>
        <div class="controls">
            <?php echo CHtml::activeTextField($user, 'email'); ?>
        </div>
    </div>
    <div class="control-group <?php if ($user->getError('password')) { ?>error<?php } ?>">
        <label class="control-label" for="signup-password"><?php echo Yii::t('User', 'Password'); ?></label>
        <div class="controls">
            <?php echo CHtml::activePasswordField($user, 'password'); ?>
        </div>
    </div>
    <div class="control-group <?php if ($user->getError('cpassword')) { ?>error<?php } ?>">
        <label class="control-label" for="signup-cpassword"><?php echo Yii::t('User', 'Confirm password'); ?></label>
        <div class="controls">
            <?php echo CHtml::activePasswordField($user, 'cpassword'); ?>
        </div>
    </div>
    <?php if (Yii::app()->user->isAdmin()) { ?>
        <div class="control-group <?php if ($user->getError('role')) { ?>error<?php } ?>">
            <?php echo CHtml::activeLabel($user, 'role', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                    $availableRoles = array_keys(Yii::app()->authManager->getAuthItems(CAuthItem::TYPE_ROLE));
                    $roles = array();
                    foreach ($availableRoles as $role) {
                        $roles[$role] = $role;
                    }
                ?>
                <?php echo CHtml::activeDropDownList($user, 'role', $roles); ?>
            </div>
        </div>
    <?php } ?>
    <div class="control-group <?php if ($user->getError('photo')) { ?>error<?php } ?>">
        <label class="control-label" for="signup-photo"><?php echo Yii::t('User', 'Photo'); ?></label>
        <div class="controls">
            <?php echo CHtml::activeFileField($user, 'photo'); ?>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button class="btn btn-primary"><?php echo Yii::t('Common', 'Sign up'); ?></button>
        </div>
    </div>
</form>
