<?php
/**
 * @var $this LogController
 * @var $logs Log[]
 * @var $pages array
 */

$this->breadcrumbs=array(
    'Logs'
);
?>
<h1><?php echo Yii::t('Common', 'Logs'); ?></h1>

<table class="table table-bordered table-striped table-hover">
    <tr>
        <th><?php echo Yii::t('Common', 'Category'); ?></th>
        <th><?php echo Yii::t('Common', 'Level'); ?></th>
        <th><?php echo Yii::t('Common', 'Message'); ?></th>
        <th><?php echo Yii::t('Common', 'Date'); ?></th>
    </tr>
    <?php foreach ($logs as $log) { ?>
        <tr>
            <td><?php echo CHtml::encode($log->category) ?></td>
            <td><?php /*switch ($log->level) {
                    default:
                    case CLogger::LEVEL_INFO:
                        $class = 'info';
                        break;
                    case CLogger::LEVEL_ERROR:
                        $class = 'error';
                        break;
                    case CLogger::LEVEL_WARNING:
                        $class = 'warning';
                        break;
                } */?>
                <span class="label label-<?php echo CHtml::encode($log->level) ?>"><?php echo CHtml::encode($log->level) ?></span>
            </td>
            <td><?php echo CHtml::encode($log->message) ?></td>
            <td class="nowrap"><?php echo Helpers::dateToAgoFormat(date('c', $log->logtime)) ?></td>
        </tr>
    <?php } ?>
    <?php if (!$logs) { ?>
        <tr>
            <td colspan="4"><?php echo Yii::t('Common', 'No data found'); ?></td>
        </tr>
    <?php } ?>
</table>
<div class="pagination pagination-right">
    <?php $this->widget(
        'LinkPager', array(
            'pages'       => $pages,
            'htmlOptions' => array(
                'class' => ''
            )
        )
    ); ?>
</div>

