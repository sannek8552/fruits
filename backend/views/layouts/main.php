<?php
/**
 *
 * main.php layout
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?php echo Yii::t('Common', 'Fruits Admin Panel'); ?></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">

	<link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/bootstrap.notify.css">
	<link rel="stylesheet" href="/css/font-awesome.css">

	<style>
		body {
			padding-top: 60px;
			padding-bottom: 40px;
		}
	</style>

	<link rel="stylesheet" href="/css/main.css">

	<script src="/js/libs/modernizr-2.6.2-respond-1.1.0.min.js"></script>

</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
	your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
	improve your experience.</p>
<![endif]-->

<!-- This code is taken from http://twitter.github.com/bootstrap/examples/hero.html -->

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
            <div class="notifications top-right"></div>
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="/"><?php echo Yii::t('Common', 'Fruits Admin Panel'); ?></a>

			<div class="nav-collapse collapse">
				<ul class="nav">
                    <li><a href="/users"><?php echo Yii::t('Common', 'Users'); ?></a></li>
                    <li><a href="/log"><?php echo Yii::t('Common', 'Logs'); ?></a></li>
                    <li><a href="/locale"><?php echo Yii::t('Common', 'Locale'); ?></a></li>
                    <li><a href="/config"><?php echo Yii::t('Common', 'Config'); ?></a></li>
				</ul>
                <ul class="nav pull-right">
                    <?php if (Yii::app()->user->isGuest) { ?>
                        <li><a href="/auth/login"><?php echo Yii::t('Common', 'Login'); ?></a></li>
                        <li><a href="/auth/signup"><?php echo Yii::t('Common', 'Sign Up'); ?></a></li>
                    <?php } else { ?>
                        <li><a href="/u/<?php echo Yii::app()->user->name ?>"><?php echo Yii::app()->user->name ?></a></li>
                        <li><a href="/auth/logout"><?php echo Yii::t('Common', 'Logout'); ?> <i class="icon-share-alt"></i></a></li>
                    <?php } ?>
                </ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
</div>

<div class="container">
    <?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links' => $this->breadcrumbs,
            'separator' => '<span class="divider">/</span>',
        )); ?>
    <div class="row-fluid">
        <?php echo $content; ?>
    </div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script src="/js/libs/bootstrap.min.js"></script>
<script src="/js/libs/bootstrap.notify.js"></script>
<script src="/js/libs/notifier.js"></script>
<script src="/js/plugins.js"></script>
<script src="/js/main.js"></script><script>
	var _gaq = [
		['_setAccount', 'UA-XXXXX-X'],
		['_trackPageview']
	];
	(function (d, t) {
		var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
		g.src = ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g, s)
	}(document, 'script'));
</script>
<script>
    $(function () {
        $('.control-group').mousedown(function () {
            $(this).removeClass('error');
        });
    });
    <?php $flash = Yii::app()->user->getFlashes();
        foreach ($flash as $key => $msg) { ?>
        Notifier.showMessages(<?php echo json_encode($msg) ?>,'<?php echo CHtml::encode($key) ?>');
    <?php } ?>
</script>
</body>
</html>
