<?php
/**
 * @var $this ConfigController
 * @var $configs Config[]
 * @var $config Config
 * @var $pages array
 */

$this->breadcrumbs=array(
    Yii::t('Common', 'Config')
);
?>
<h1><?php echo Yii::t('Common', 'Config'); ?></h1>
<form id="lang-form" method="post" class="form-horizontal">
    <?php if ($config->hasErrors()) { ?>
        <div class="alert alert-error">
            <?php echo CHtml::errorSummary($config, false); ?>
        </div>
    <?php } ?>
    <div class="control-group <?php if ($config->getError('name')) { ?>error<?php } ?>">
        <?php echo CHtml::activeLabel($config, 'name', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo CHtml::activeTextField($config, 'name'); ?>
        </div>
    </div>
    <div class="control-group <?php if ($config->getError('value')) { ?>error<?php } ?>">
        <?php echo CHtml::activeLabel($config, 'value', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo CHtml::activeTextField($config, 'value'); ?>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button class="btn btn-success"><?php echo Yii::t('Common', $config->id ? 'Save' : 'Add'); ?></button>
            <?php if ($config->id) { ?>
                <a class="btn" href="/config"><?php echo Yii::t('Common', 'Cancel'); ?></a>
            <?php } ?>
        </div>
    </div>
</form>
<?php if (!$config->id) { ?>
    <table class="table table-bordered table-striped table-hover">
        <tr>
            <th><?php echo Yii::t('Common', 'ID'); ?></th>
            <th><?php echo Yii::t('Common', 'Name'); ?></th>
            <th><?php echo Yii::t('Common', 'Value'); ?></th>
            <th style="width: 100px; text-align: center;"><?php echo Yii::t('Common', 'Actions'); ?></th>
        </tr>
        <?php foreach ($configs as $item) { ?>
            <tr>
                <td><?php echo CHtml::encode($item->id) ?></td>
                <td><?php echo CHtml::encode($item->name) ?></td>
                <td><?php echo CHtml::encode($item->value) ?></td>
                <td>
                    <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                            <?php echo Yii::t('Common', 'Action'); ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/config/<?php echo CHtml::encode($item->id) ?>"><?php echo Yii::t('Common', 'Edit'); ?></a></li>
                            <li><a href="/config/delete/<?php echo CHtml::encode($item->id) ?>" onclick="return confirm('Are you sure?');"><?php echo Yii::t('Common', 'Delete'); ?></a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        <?php } ?>
        <?php if (!$configs) { ?>
            <tr>
                <td colspan="4"><?php echo Yii::t('Common', 'No data found'); ?></td>
            </tr>
        <?php } ?>
    </table>
    <div class="pagination pagination-right">
        <?php $this->widget(
            'LinkPager', array(
                'pages'       => $pages,
                'htmlOptions' => array(
                    'class' => ''
                )
            )
        ); ?>
    </div>

<?php } ?>
