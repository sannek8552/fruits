$(function () {
    var scrollOptions = {
        data: {
            filter: $('.search-query').val()
        },
        offset: 5,
        limit: 5,
        url: '/user/ajaxuser',
        loadingHtml: 'Loading...',
        dataType: 'html',
        success: function (res) {
            $(this).append(res);
        }
    };
    $('.scrollable').scrollable(scrollOptions);

    //search user handler
    $('.user-search-panel').on('submit', function (e) {
        e.preventDefault();

        $('.scrollable').scrollable('destroy');
        var filter = $('.search-query').val();
        $('.user-list').load(
            '/user/ajaxuser',
            {
                filter: filter
            },
            function () {
                scrollOptions.data.filter = $('.search-query').val();
                scrollOptions.offset = 5;
                $('.scrollable').scrollable(scrollOptions);
            }
        );
    });

    $(document).on('click', 'button.delete-user', deleteUser);
});


function deleteUser () {

    if (!confirm('Are you sure?')) {
        return;
    }
    var $btn  = $(this).button('loading'),
        $cnt  = $btn.closest('.item');
    var params = {
        id:  $cnt.data('userId')
    };

    setTimeout(function() {
        $.post(
            '/user/ajaxdeleteuser',
            params,
            function () {
                $btn.button('reset');
                $cnt.slideUp(500, function () {$(this).remove()});
            },
            'json'
        );
    }, 1000);
}



