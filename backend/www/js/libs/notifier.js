/**
 * @class Notifier
 */
var Notifier = {

	containerClass: 'notifications',

	/**
	 *
	 * @param {Array|Object|String} messages
	 * @param {String} type (success, error, warning, info)
	 */
	showMessages: function (messages, type) {
		//$('#' + this.containerClass).empty();
		var self = this;
		if ($.isArray(messages) || $.isPlainObject(messages)) {
			jQuery.each(messages, function(index, msg) {
				self.showMessage(msg, type);
			});
		} else {
			this.showMessage(messages, type);
		}
	},

	/**
	 *
	 * @param {String} message
	 * @param {String} type (success, error, warning, info)
	 * @private
	 */
	showMessage: function (message, type) {
		message = '<span>' + message + '</span>';

		var $container = $('.' + this.containerClass);
		$container.notify({
			message : {
				html : message,
				text : message
			},
			type    : type,
			fadeOut : {
				enabled : true,
				delay   : 8000
			}
		}).show();
	},


	/**
	 *
	 * @param {Array|Object|String} message
	 */
	showError: function (message) {
		this.showMessages(message, 'error');
	},

	/**
	 *
	 * @param {Array|Object|String} message
	 */
	showSuccess: function (message) {
		this.showMessages(message, 'success');
	},

	/**
	 *
	 * @param {Array|Object|String} message
	 */
	showWarning: function (message) {
		this.showMessages(message, 'warning');
	},

	/**
	 *
	 * @param {Array|Object|String} message
	 */
	showInfo: function (message) {
		this.showMessages(message, 'info');
	}
}
